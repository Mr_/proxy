#coding=utf8

import filtration.base
import services.redis_client
        
from settings import REDIS_CONFIG

class Redis(filtration.base.Base):

    def __init__(self, setting=None):
        super(Redis, self).__init__()
        setting = REDIS_CONFIG if setting is None else setting
        self.db = services.redis_client.Redis_client(**setting)

    def add(self, key, values):
        return self.db.sadd(key, values)

    def exists(self, key, value):
        return self.db.sismember(key, value)

    def length(self, key):
        return self.db.scard(key)

CLS_NAME = Redis
        