import time
import json
import datetime

import utils.coroutine
import net.request
import utils.proxy_check
import utils.builtin as bu
import storage.dbinterface

from settings import PROXY_BASE

'''
全网ip扫描
'''
class Ip_scan(object):

    CFG_PATH = 'sources'
    CFG_NAME = 'port.cfg'

    def __init__(self):
        super(Ip_scan, self).__init__()

    def mk_seeds(self, ip):
        ports = bu.load_json(self.CFG_PATH, self.CFG_NAME)['ports']
        return ({'proxy': f'{ip}.{x}:{port}'} for x in range(256) for port in ports)

    def fix_result(self, result):
        '''
        丰富result的字段
        '''
        now = datetime.datetime.now()
        base_info = {
            'inside': 1,
            'country': '',
            'region': '',
            'city': '',
            'isp': '',
            'collect_time': now.strftime('%Y-%m-%d %H:%M:%S'),
            'collect_date': now.strftime('%Y-%m-%d'),
            'source': 'IP SCAN',
        }
        for item in result:
            item.update(base_info)
            item['sign'] = item['proxy']
        return result

    def get_ip(self):
        dri = net.request.Request()
        html = dri.req(f'{PROXY_BASE}/scan_ip', method='POST')
        resp = json.loads(html)
        return resp['data']

    def run(self):
        db = storage.dbinterface.DBInterface()
        pool = utils.coroutine.Coroutine(200)
        while True:
            start = datetime.datetime.now()
            seeds = self.mk_seeds(self.get_ip())
            result = utils.proxy_check.checks(pool, seeds, 'Ip scan add : ')
            result = self.fix_result(result)
            utils.proxy_check.check_pos(pool, result)
            db.push_data('proxies', result)
            end = datetime.datetime.now()
            sec = (end - start).seconds
            t = end.strftime('%Y-%m-%d %H:%M:%S')
            res_len = len(result)
            speed = round(res_len / sec * 86400)
            print(f'{t} num: {res_len}, {sec}s , speed: {speed}/day')
            print('*'*30)
            time.sleep(5)

if __name__ == '__main__':
    Ip_scan().run()
    # Ip_scan().init_cfg()
