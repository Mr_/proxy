# coding=utf8

import json
import time

import requests

try:
    from settings import PROXY_GET, PROXY_DEL
except Exception as e:
    PROXY_GET, PROXY_DEL = '', ''

def req(http='', https='', inside=1, size=20, interval=3, retry=50):
    url = PROXY_GET.format(
            http=http,
            https=https,
            inside=inside,
            size=size
            )
    for x in range(retry):
        try:
            proxy = requests.get(url).content
        except Exception as e:
            time.sleep(interval)
            continue
        # import pdb; pdb.set_trace()
        datas = json.loads(proxy)['datas']
        if len(datas) == 0:
            time.sleep(interval)
            continue
        return datas

def http(inside=1, size=20):
    return req(http=1, inside=inside, size=size)

def https(inside=1, size=20):
    return req(https=1, inside=inside, size=size)

def proxies():
    h_p = http()
    hs_p = https()
    return h_p.update(hs_p)
