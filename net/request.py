import json
import time
import queue
import random

import requests

import net.headers

from net.proxy import http, https

from requests.packages.urllib3.exceptions import InsecureRequestWarning
# 禁用安全请求警告
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class Request:
    '''
    网络请求工具，基于request库进行二次封装，强化了代理功能，可用于非渲染网络请求
    '''

    USE_PROXY = False

    def __init__(self, session=False):
        self.ips = {'http': queue.Queue(), 'https': queue.Queue()}
        self.s_sion = session
        if session:
            self.ss = requests.Session()
        else:
            self.ss = requests
        self.status_code = (200, 302)
        self.req_funcs = {
            'GET': self.get,
            'POST': self.post,
            'OPTIONS': self.options,
        }

    def get_cookies(self, is_str=False):
        if self.s_sion:
            ck = self.ss.cookies.get_dict()
            if is_str:
                ck = '; '.join((f'{k}={v}'for k,v in ck.items()))
            return ck
        return None

    def set_cookies(self, cookies):
        if self.s_sion and isinstance(cookies, dict):
            self.ss.cookies.update(cookies)

    def req(self, url, method='GET', params=None, header=None, 
            retry_time=12, timeout=10, retry_flag=None, 
            appear_flag=None, appear_flag_or=None, retry_interval=5, verify=False,
            inside=1, min_len=None, proxies=None, log_err=False,
            proxy=None, return_handle=False, files=None, encoding=None,
            redirects=True, byte=False, os_agent=False, read=True):
        '''
        访问URL,获取网页内容

        Parameters
        ----------
        url: str
            想要访问的网址
        method: str
            访问方式(GET/POST/OPTIONS)
        params: dict
            访问参数
        header: dict
            head信息
        retry_time: int
            重试次数，如果访问失败时重复尝试的次数
        timeout: int
            超时时间
        retry_flag: tuple/list
            如果出现flag指定的关键字，则重新请求一次
        appear_flag: tuple/list
            如果没有出现flag所有指定的关键字，则重新请求一次
        appear_flag_or: tuple/list
            只要出现flag指定的任意关键字，则无需重新请求一次
        retry_interval: int
            重新请求间隔时长
        verify: bool
            是否验证SSL证书
        inside: int
            代理ip的inside要求
        min_len: int
            返回文本最短长度，如果无法超过这个长度，则重新请求一次
        proxies: dict
            指定一个代理 eg:{'http': '127.0.0.1:80'}
        log_err: bool
            是否打印错误信息
        proxy: bool
            本次请求是否使用代理
        return_handle: bool
            是否返回resp对象，False直接返回html文本
        files: file
            文件对象，上传文件时使用
        encoding: str
            文本编码
        redirects: bool
            是否允许重定向
        byte: bool
            是否返回byte格式文本
        os_agent: bool
            是否使用移动端user-agent
        read: bool
            是否读取html内容
        Returns
        -------
            网页内容
        '''
        headers = net.headers.header(os_agent=os_agent)
        if header is not None:
            headers.update(header)
        postdatas = {
            'headers': headers,
            'timeout': timeout,
            'verify': verify,
            'files': files,
            'allow_redirects': redirects,
        }
        proxy = self.USE_PROXY if proxy is None else proxy
        req_func = self.req_funcs[method.upper()]
        check_funcs = self.get_check_func(retry_flag, appear_flag,
            appear_flag_or)
        while retry_time >= 0:
            if proxies is not None:
                postdatas['proxies'] = proxies
            elif proxy:
                postdatas['proxies'] = self.get_proxys(url, inside=inside)
            try:
                html = req_func(url, postdatas, params)
                if encoding is not None:
                    html.encoding = encoding
                if read:
                    content = html.content if byte else html.text
                if html.status_code not in self.status_code:
                    raise Exception(f'Err code {html.status_code}, content:{content}')
                for flag, check_func in zip(*check_funcs):
                    check_func(flag, content, postdatas.get('proxies', None))
                if min_len is not None:
                    if len(content) <= min_len:
                        raise Exception(f'Too less content {len(content)}: {content}')
                try:
                    return html if return_handle else content
                except Exception as e:
                    return content
            except Exception as e:
                if log_err:
                    print(e)
                retry_time -= 1
                time.sleep(retry_interval)

    def get_check_func(self, retry_flag, appear_flag, appear_flag_or):
        '''
        把flags和检查函数打包成tuple
        '''
        flags, funcs = [], []
        if retry_flag is not None:
            flags.append(retry_flag)
            funcs.append(self.check_retry_flag)
        if appear_flag is not None:
            flags.append(appear_flag)
            funcs.append(self.check_appear_flag)
        if appear_flag_or is not None:
            flags.append(appear_flag_or)
            funcs.append(self.check_appear_flag_or)
        return (flags, funcs)

    def check_retry_flag(self, flag, content, proxy):
        '''
        如果content中出现flag包含的关键字，抛出异常，重新尝试请求
        '''
        for f in flag:
            if f in content:
                raise Exception(f'Retry_flag:{f} {proxy}')

    def check_appear_flag(self, flag, content, proxy):
        '''
        如果content中未出现flag所有包含的关键字，抛出异常，重新尝试请求
        '''
        for f in flag:
            if f not in content:
                raise Exception(f'Appear_flag:{f} {proxy}')

    def check_appear_flag_or(self, flag, content, proxy):
        '''
        如果content中未出现特定关键字，抛出异常，重新尝试请求，
        与check_appear_flag相比，此方法只需要有一个命中就不会抛出异常
        '''
        for f in flag:
            if f in content:
                break
        else:
            raise Exception(f'Appear_flag_or:{f} {proxy}')

    def get(self, url, postdatas, params):
        '''
        get请求
        '''
        postdatas['params'] = params
        return self.ss.get(url, **postdatas)

    def post(self, url, postdatas, params):
        '''
        post请求
        '''
        content_type = postdatas['headers'].get('content-type', '')
        if 'application/json' in content_type.lower():
            # if isinstance(params, dict):
            #     params = json.dumps(params)
            postdatas['json'] = params
        else:
            postdatas['data'] = params
        return self.ss.post(url, **postdatas)

    def options(self, url, postdatas, params):
        '''
        options请求
        '''
        return self.ss.options(url, **postdatas)

    def get_proxys(self, url, inside):
        key = 'https' if url.startswith('https') else 'http'
        val = self.ips[key]
        if val.empty():
            if key == 'https':
                datas = https(inside=inside)
            else:
                datas = http(inside=inside)
            [val.put(x['proxy'])for x in datas]
        return {key: '%s://%s' %(key, val.get())}

    def test_ip(self, proxy=None):
        params = {
            'timeout': 8,
            'retry_time': 3,
            'retry_interval': 0,
            'log_err': True,
        }
        http_url, https_url = 'http://httpbin.org/ip', 'https://httpbin.org/ip'
        if proxy is None:
            proxy_bak, self.USE_PROXY = self.USE_PROXY, True
            print('http:', self.req(http_url))
            print('https:', self.req(https_url))
            self.USE_PROXY = proxy_bak
        else:
            page = self.req(http_url, proxies={'http':f'http://{proxy}'}, **params)
            print(f'http:{page}')
            page = self.req(https_url, proxies={'https':f'https://{proxy}'}, **params)
            print(f'https:{page}')

    def test_url(self, url, proxy):
        proxies = {
            'http':'http://{}'.format(proxy),
            'https':'https://{}'.format(proxy),
            }
        page = self.req(url, proxies=proxies)
        print(('http:', page))

    def close(self):
        pass
