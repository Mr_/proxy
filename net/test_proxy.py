import json

import utils.coroutine
import net.request

dri = net.request.Request()

num = 100
pool = utils.coroutine.Coroutine(num)

def test(ip):
    '''
    测试代理IP质量，包含1个HPPT代理IP和1个HPPTS代理IP
    '''
    result = {}
    urls = {
        'http': 'http://httpbin.org/ip',
        'https': 'https://httpbin.org/ip'
    }
    for k,url in urls.items():
        html = dri.req(url, inside='')
        try:
            data = json.loads(html)
            ori = data.get('origin', '')
            result[k] = (ori, ip in ori)
            print(ip in ori, k, ori)
        except Exception as e:
            print(f'Err: {k} request')
            return None
    return result

def run(real_ip):
    dri.USE_PROXY = True
    http, https = 0, 0
    datas = pool.run(test, (real_ip for x in range(num)))
    for itme in datas:
        if not itme['http'][1]:
            http += 1
        if not itme['https'][1]:
            https += 1
    print(f'Activity http proxy: {http/num*100}% {http}/{num}')
    print(f'Activity https proxy: {https/num*100}% {https}/{num}')

def get_local_ip():
    '''
    本地IP
    '''
    html = dri.req('http://httpbin.org/ip')
    data = json.loads(html)
    ori = data.get('origin', '')
    return ori.split(',')[0]

if __name__ == '__main__':
    ip = get_local_ip()
    run(ip)
    print(f'Local ip: {ip}')
    