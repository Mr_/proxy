#coding=utf-8

import os
import re
import json
import time
import random
import socket
import urllib.request, urllib.parse, urllib.error
import datetime
import traceback
import threading

import selenium.webdriver
import selenium.common.exceptions

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import settings
import net.headers

from utils.builtin import timestamp

PROPERTYS = {
    'id':By.ID,
    'name':By.NAME,
    'css':By.CSS_SELECTOR,
    'class_name':By.CLASS_NAME,
}


class FirefoxDriver(selenium.webdriver.Firefox):
    def __init__(self, *args, **kwargs):
        super(FirefoxDriver, self).__init__(*args,
            executable_path=settings.FIREFOXDRIVER_PATH, **kwargs)
        self.handlers = {}


class ChromeDriver(selenium.webdriver.Chrome):
    def __init__(self, *args, **kwargs):
        super(ChromeDriver, self).__init__(*args,
                executable_path=settings.CHROMEDRIVER_PATH, **kwargs)
        self.handlers = {}


class Phantomjs(selenium.webdriver.PhantomJS):
    def __init__(self, *args, **kwargs):
        super(Phantomjs, self).__init__(*args, **kwargs)
        self.handlers = {}


def create_driver(profile_path, driver_type='firefox', hide=False):
    options = None
    if driver_type == 'firefox':
        if profile_path is None:
            profile = None
        else:
            profile = selenium.webdriver.FirefoxProfile(profile_path)
        selenium.webdriver.DesiredCapabilities.FIREFOX['firefox.page.settings.userAgent'] = net.headers.agent()
        if hide:
            from selenium.webdriver.firefox.options import Options
            options = Options()
            options.add_argument('-headless')
        driver = FirefoxDriver(firefox_profile=profile, timeout=600, firefox_options=options)
    elif driver_type == 'chrome':
        if hide:
            from selenium.webdriver.chrome.options import Options
            options = Options()
            options.add_argument('-headless')
            # root用户下运行代码需添加这一行
            options.add_argument('--no-sandbox')
        driver = ChromeDriver(chrome_options=options)
    elif driver_type == 'phantomjs':
        for key, value in list(net.headers.header().items()):
            capability_key = 'phantomjs.page.customHeaders.{}'.format(key)
            selenium.webdriver.DesiredCapabilities.PHANTOMJS[capability_key] = value
        driver = Phantomjs()
    current_handler = driver.window_handles[-1]
    return driver, current_handler

def create_new_window(driver):
    body = driver.find_element_by_tag_name("body")
    body.send_keys(Keys.CONTROL + 'n')
    current_handler = driver.window_handles[-1]
    return current_handler

def phantomjs():
    return Webdriver(driver_type='phantomjs')

def firefox(hide=False):
    return Webdriver(driver_type='firefox', hide=hide)

def chrome(hide=False):
    return Webdriver(driver_type='chrome', hide=hide)


class Webdriver(object):

    NO_DRIVER_SIGN = False

    def __init__(self, profilepath=None, downloader=None,
                    driver_type='firefox', hide=False):
        self.id = id(self)
        self.profilepath = None
        self.driver_regs = {}
        self.driver_type = driver_type
        self.hide = hide
        if downloader is not None:
            self.lock = downloader.lock
            self.driver_regs[self.id] = downloader.driver_regs[downloader.id]
            driver = downloader.driver_regs[downloader.id]['driver']
            handler = create_new_window(driver)
        else:
            self.lock = threading.Lock()
            self.profilepath = profilepath
            driver, handler = create_driver(self.profilepath, self.driver_type, hide=hide)
            self.driver_regs[self.id] = {'driver':driver, 'master':True,}
        driver.handlers[self.id] = handler

    def time_to_wait(self, t):
        """
        设置全局超时，会影响网页打开超时和遍历元素超时
        param: 
            t: int second
        """
        self.driver.implicitly_wait(t)

    @property
    def is_master(self):
        return self.driver_regs[self.id]['master']

    @property
    def driver(self):
        return self.driver_regs[self.id]['driver']

    @property
    def page_source(self):
        return self.driver.page_source

    @property
    def windows_num(self):
        return len(self.driver.window_handles)

    def get_wait_val(self, key):
        if key not in PROPERTYS:
            raise Exception('key must the one of id、name、css、class_name')
        return PROPERTYS[key]

    def until(self, condition, timeout):
        try:
            WebDriverWait(self.driver, timeout).until(condition)
        except Exception as e:
            return False
        return True

    def until_not(self, condition, timeout):
        try:
            WebDriverWait(self.driver, timeout).until_not(condition)
        except Exception as e:
            return False
        return True

    def wait_visiblity(self, key, value, timeout=10, contrary=False):
        """
        智能等待某个元素在网页中成为可见状态
        params:
        type:元素某个属性的类型：id、name、css、class_name
        value:属性的值
        timeout:超时，单位秒
        contrary:反效果
        """
        condition = EC.visibility_of_element_located((
            self.get_wait_val(key),
            value
        ))
        if contrary:
            return self.until_not(condition, timeout)
        return self.until(condition, timeout)
    
    def wait_appear(self, key, value, timeout=10):
        """
        智能等待某个元素出现
        params:
        type:元素某个属性的类型：id、name、css、class_name
        value:属性的值
        timeout:超时，单位秒
        """
        condition = EC.presence_of_element_located((
            self.get_wait_val(key),
            value
        ))
        return self.until(condition, timeout)

    def wait_text(self, key, value, text, timeout=10):
        """
        智能等待某个元素里的文字出现
        params:
        type:元素某个属性的类型：id、name、css、class_name
        value:属性的值
        timeout:超时，单位秒
        """
        condition = EC.text_to_be_present_in_element((
            self.get_wait_val(key),
            value),
            text
        )
        return self.until(condition, timeout)

    def wait_title_contains(self, word, timeout=10):
        '''
        智能等待标题里包含指定字符
        params:
        word:字符
        timeout:超时，单位秒
        '''
        condition = EC.title_contains(word)
        return self.until(condition, timeout)

    def wait_ele(self, target, timeout=5, interval=0.5):
        '''
        智能等待指定元素出现
        params:
        target: dict 
                key: id、name、css、class_name
                value : str
        timeout:超时，单位秒
        '''
        end_time = time.time() + timeout
        while True:
            ele = self.get_element(**target)
            if ele is not None:
                return True
            time.sleep(interval)
            if time.time() > end_time:
                break
        return False

    def wait_re(self, txt, timeout=5, interval=0.5):
        '''
        params:
        target: txt
        timeout:超时，单位秒
        '''
        end_time = time.time() + timeout
        while True:
            match = re.findall(txt, self.page_source)
            if len(match) > 0:
                return True
            time.sleep(interval)
            if time.time() > end_time:
                break
        return False

    def clean_dirver_sign(self):
        shell = "Object.defineProperty(navigator,'webdriver',{get:function(){return false;}});"
        self.run_script(shell)

    def run_script(self, shell):
        self.driver.execute_script(shell)
        
    def is_setup(self):
        try:
            # self.driver.switch_to_window(self.driver.handlers[self.id])
            return self.driver.current_url.startswith('http')
        except socket.error:
            return False

    def wait_page(self, text=None, untext=None, timeout=10):
        for x in range(timeout):
            if text and text in self.page_source:
                return True
            if untext and untext not in self.page_source:
                return True
            time.sleep(0.5)
        return False

    def switch_window(self, index):
        if index >= 0 and len(self.driver.window_handles) > index:
            self.driver.switch_to_window(self.driver.window_handles[index])
            return True
        else:
            return False

    def reset_driver(self):
        if not self.is_master:
            return
        ids = list(self.driver.handlers.keys())
        try:
            self.driver.quit()
        except OSError:
            pass
        driver, handler = create_driver(self.profilepath, self.driver_type, hide=self.hide)
        self.driver_regs[self.id]['driver'] = driver
        for _id in ids:
            if _id == self.id:
                _handler = handler
            else:
                _handler = create_new_window(self.driver)
            self.driver.handlers[_id] = _handler

    def switch_profile(self, profile_paths):
        if not self.is_master:
            return
        if self.profilepath in profile_paths:
            index = profile_paths.index(self.profilepath)
            next_index = (index + 1)%len(profile_paths)
            self.profilepath = profile_paths[next_index]
        else:
            self.profilepath = profile_paths[0]
        with self.lock:
            self.reset_driver()

    def hold_and_move(self, target, tracks):
        ele = self.get_element(**target)
        if ele is None:
            return False
        try:
            ActionChains(self.driver).click_and_hold(ele).perform()
            for track in tracks:
                ActionChains(self.driver).\
                    move_by_offset(xoffset=track, yoffset=0).perform()
            ActionChains(self.driver).release(on_element=ele).perform()
        except Exception as e:
            print((traceback.format_exc()))
            return False
        return True

    def click_ele_with_pos(self, target, points):
        ele = self.get_element(**target)
        if ele is None:
            return False
        for point in points:
            action = ActionChains(self.driver)
            p = [int(point[0]), int(point[1])]
            action.move_to_element_with_offset(ele, *p).perform()
            ActionChains(self.driver).click().perform()
            sleep_time = random.randint(6, 11)/10.0
            time.sleep(sleep_time)
        return True

    def get_element(self, name=None, id=None, css=None, link_txt=None,
            class_name=None):
        try:
            if name is not None:
                return self.driver.find_element_by_name(name)
            if id is not None:
                return self.driver.find_element_by_id(id)
            if css is not None:
                return self.driver.find_element_by_css_selector(css)
            if link_txt is not None:
                return self.driver.find_element_by_link_text(link_txt)
            if class_name is not None:
                return self.driver.find_element_by_class_name(class_name)
        except Exception as e:
            pass
        return None

    def send(self, key, value):
        '''
        向某个元素填充字符串
        params:
        key: dict
            name:根据name选择元素；
            id:根据id选择元素；
            css:使用css选择器选择元素;
            link_txt:根据URL文本选择元素;
            class_name:根据类名选择元素;
        value:填充内容
        '''
        ele = self.get_element(**key)
        ele.clear()
        ele.send_keys(value)

    def click(self, target):
        '''
        点击某个元素
        params:
        name:根据name选择元素；
        id:根据id选择元素；
        css:使用css选择器选择元素;
        link_txt:根据URL文本选择元素;
        class_name:根据类名选择元素;
        '''
        ele = self.get_element(**target)
        try:
            ele.click()
            return True
        except Exception as e:
            return False

    def init_cookies(self, cks):
        '''
        初始化cookies，会把原有的cookies全部清空
        '''
        self.driver.delete_all_cookies()
        if isinstance(cks, list):
            for ck in cks:
                if 'name' in ck and 'value' in ck:
                    self.add_cookie(ck)
        elif isinstance(cks, dict):
            for k, v in list(cks.items()):
                self.add_cookie({'name': k, 'value': v})

    def refresh(self):
        self.driver.refresh()

    def add_cookie(self, ck):
        '''
        插入cookie
        params: ck(dict) eg {'name': xx, 'value': xx}
        '''
        self.driver.add_cookie(ck)

    def verify_content(self, page):
        if '错误的网址' in page:
            return False
        if 'ERR_INTERNET_DISCONNECTED' in page:
            return False
        return True
        
    def get(self, url, try_times=15, len_min=0, interval=5, timeout=30):
        self.driver.set_page_load_timeout(timeout)
        for _i in range(try_times):
            try:
                page = self._get(url)
            except:
                print('time out retry')
                page = ''
            if len(page) > len_min and self.verify_content(page):
                return page
            time.sleep(interval)
        return None

    def req(self, url, param=None, method='GET', header=None, 
            try_times=5, len_min=100, interval=5):
        if not self.is_setup():
            raise Exception('You need use get() before use req().')
        param = {} if param is None else param
        for _i in range(try_times):
            try:
                page = self.request(url, param, method, header)
            except:
                page = ''
            if len(page) > len_min and self.verify_content(page):
                return page
            time.sleep(interval)
        return self.request(url, param, method, header)

    def _get(self, url):
        with self.lock:
            try:
                # self.driver.switch_to_window(self.driver.handlers[self.id])
                self.driver.get(url)
            except socket.error:
                self.reset_driver()
                self.driver.switch_to_window(self.driver.handlers[self.id])
                self.driver.get(url)
            if self.NO_DRIVER_SIGN:
                self.clean_dirver_sign()
            return self.page_source
        return None

    def request(self, url, form, method='GET', header=None):
        js = """function request(path, params, method, header) {
                    var xhr = new XMLHttpRequest();
                    xhr.open(method, path, false);
                    if (typeof(params) == 'string') {
                        var formData = params;
                        xhr.setRequestHeader("cache-control","no-cache"); 
                        xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                    }
                    else {
                        var formData = new FormData();
                        for(var key in params) {
                            formData.append(key, params[key]);
                        }
                    }
                    if (header != null) {
                        for (var key in header) {
                            xhr.setRequestHeader(key, header[key]);
                        }
                    }
                    xhr.send(formData);
                    var result = "";
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            result = xhr.responseText;
                        }
                    }
                    return result;
                }
                var method = %s;
                var url = %s;
                var form = %s;
                var header = %s;
                return request(url, form, method, header);"""

        if type(form) == str:
            if method.upper() == 'GET':
                url = url + form
            form = '"'.join(('', form.replace('"', '\\"'), ''))
        else:
            if method.upper() == 'GET':
                url = url + urllib.parse.urlencode(form)
            form = str(form)
        _method = '"'.join(('', method, ''))
        _url = '"'.join(('', url, ''))
        if header is None:
            header = 'null'
        else:
            header = str(header)
        page = self.run_script(js%(_method, _url, form, header))
        return page

    def draw_ele(self, eles):
        '''
        对元素进行截图
        params:
        eles list 要截图的元素参数，{'ele': {}, 'css': ''}
        '''
        result = []
        path = 'imgs'
        if not os.path.exists(path):
            os.mkdir(path)
        for ele in eles:
            if 'js' in ele:
                self.run_script(ele['js'])
            tar = self.get_element(**ele['ele'])
            img_path = '%s/%d_%d.png' %(path, timestamp(), random.randint(1, 999999))
            tar.screenshot(img_path)
            result.append(img_path)
        return result

    def close(self):
        if not self.is_master:
            return
        with self.lock:
            try:
                self.driver.quit()
            except OSError:
                pass

    def close_tab(self):
        self.driver.close()
