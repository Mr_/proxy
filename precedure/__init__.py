#coding=utf8

from functools import reduce

def parse_class(cls):
    """
    隐藏的解码函数
    :param cls:
    :return:
    """
    meta = dict(list(zip("ABCDEFGHIZ", list(range(10)))))
    num = reduce(lambda x, y: x + str(meta.get(y)), cls, "")
    return int(num) >> 3