#coding=utf8
import utils.coroutine

import time

import pybulk

import net.request
import net.webdriver
import utils.builtin


class Base(object):

    BASE_URL=''
    SOURCE = 'base'
    PAGE_VAR = 'page'
    INTERVAL = 1
    STARTPAGE = 1
    MAXPAGE = 999999
    VERIFY_NUM = 100
    PUSH_NUM = 100
    DRIVER_TYPE = 'phantomjs'
    FILTER_MODULE = 'filtration.redis'
    # STORAGE_MODULE = 'storage.dbinterface'
    STORAGE_TABLE = None
    FILTER_KEY = None
    USE_BROWSER = False
    USE_SESSION = False
    SIGN = None
    _driver = None
    _pool = None
    USE_PROXY = False
    storage = None
    EMPTY_COUNTER = 0
    EMPTY_NUM = 3
    PROFILEPATH = None
    POOL_SIZE = None

    def __init__(self):
        if self.FILTER_KEY is not None:
            self.filter = utils.builtin.get_module(self.FILTER_MODULE).CLS_NAME()
        # self.storage = utils.builtin.get_module(self.STORAGE_MODULE).CLS_NAME()
        self.storage = pybulk.client()

    @property
    def pool(self):
        if self.POOL_SIZE is None:
            return None
        if self._pool is None:
            self._pool = utils.coroutine.Coroutine(self.POOL_SIZE)
        return self._pool

    @property
    def driver(self):
        if self._driver is None:
            if self.USE_BROWSER:
                self._driver = net.webdriver.Webdriver(
                    profilepath=self.PROFILEPATH,
                    driver_type=self.DRIVER_TYPE,
                )
            else:
                self._driver = net.request.Request(self.USE_SESSION)
                self._driver.USE_PROXY = self.USE_PROXY
        return self._driver

    @property
    def default_params(self):
        now = time.localtime()
        return {
            'collect_time': time.strftime('%Y-%m-%d %H:%M:%S', now),
            'collect_date': time.strftime('%Y-%m-%d', now),
            'source': self.SOURCE,
        }

    def setup(self):
        if not self.driver:
            return
        try:
            self.driver.get(self.BASE_URL)
        except Exception:
            pass

    def run(self):
        self.update({})

    def download(self):
        raise NotImplementedInterface

    def parse(self):
        raise NotImplementedInterface

    def data_check(self, data):
        pass

    def pre_update(self):
        pass

    def to_json(self, data):
        return utils.builtin.to_json(data)

    def update(self, postdict):
        self.pre_update()
        add_list = []
        verify_counter = 0
        # add_counter = 0
        for cur_page in range(self.STARTPAGE, self.MAXPAGE+1):
            postdict[self.PAGE_VAR] = cur_page
            results = self.crawl(postdict)
            if self.need_break(results):
                break
            # if isinstance(results, bool):
            #     if not results:
            #         break
            # else:
            #     if not results:
            #         self.EMPTY_COUNTER += 1
            #         if self.EMPTY_COUNTER >= self.EMPTY_NUM:
            #             break
            #     else:
            #         self.EMPTY_COUNTER = 0
            verify_counter += len(results)
            add_signs = set()
            if self.SIGN is not None:
                add_signs = set(x[self.SIGN] for x in add_list)
            for result in results:
                self.data_check(result)
                data_sign = None if self.SIGN is None else result[self.SIGN]
                if data_sign is not None and data_sign in add_signs:
                    continue
                if self.exists(data_sign):
                    continue
                add_list.append(result)
                if data_sign is not None:
                    # add_signs由add_list的数据组成，但results可能也有重复数据，
                    # 所以这里add_signs需要加入新数据
                    add_signs.add(data_sign)
                # add_counter += 1
            print(('%s: current page: %d'%(self.SOURCE, cur_page)))
            if verify_counter > self.VERIFY_NUM:
                # if add_counter == 0:
                if len(add_list) == 0:
                    # 当累计的verify_counter超过VERIFY_NUM，仍然没有需要提交的值，
                    # 说明重复太多，可能没有新数据了，停止程序
                    break
                else:
                    verify_counter = 0
                    # add_counter = 0
            if len(add_list) >= self.PUSH_NUM:
                self.save(add_list)
                self.update_filter(add_list)
                add_list = []
            time.sleep(self.INTERVAL)
        self.save(add_list)
        self.update_filter(add_list)
        return True

    def need_break(self, results):
        if isinstance(results, bool):
            if not results:
                return True
        else:
            if len(results) > 0:
                self.EMPTY_COUNTER = 0
                return False
            else:
                self.EMPTY_COUNTER += 1
                if self.EMPTY_COUNTER >= self.EMPTY_NUM:
                    return True
                else:
                    return False

    def update_filter(self, add_list):
        if self.FILTER_KEY is None:
            return
        signs = (x[self.SIGN] for x in add_list)
        self.filter.add(self.FILTER_KEY, signs)

    def exists(self, data_sign):
        if self.FILTER_KEY is None:
            return False
        if self.data_sign is None:
            return False
        if self.filter.exists(self.FILTER_KEY, data_sign):
            return True
        return False

    def save(self, data_list):
        self.storage.push_data(self.STORAGE_TABLE, data_list)

    def crawl(self, postdata):
        page = self.download(postdata)
        result = self.parse(page)
        return result

    def request(self, url, **kwargs):
        return self.driver.req(url, **kwargs)

    def get_page(self, url, **kwargs):
        return self.driver.get(url, **kwargs)

    def get_md5(self, str_list):
        if not isinstance(str_list, list):
            str_list = [str_list]
        for index, _str in enumerate(str_list):
            if isinstance(_str, str):
                str_list[index] = _str.encode('utf-8')
        md5_source = '-'.join(str_list)
        return utils.builtin.md5(md5_source)

    def close(self):
        if self._driver is not None:
            self._driver.close()
            self._driver = None
        self.filter = None
        try:
            self.storage.close()
        except:
            pass
