#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check


class Crossincode(precedure.base.Base):

    SOURCE = 'crossincode'
    SEARCH_URL = 'http://lab.crossincode.com/proxy/'
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    USE_PROXY = True
    MAXPAGE = 1
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Crossincode add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self):
        page = self.request(
            self.SEARCH_URL,
            timeout=8,
            retry_interval=0,
            retry_time=60,
        )
        return page
        
    def parse(self):
        html = self.download()
        result = []
        bs = bs4.BeautifulSoup(html, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            if len(tds) == 0:
                continue
            ip = tds[0].text.strip()
            port = tds[1].text.strip()
            proxy = ':'.join((ip, str(port)))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Crossincode().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 15}]