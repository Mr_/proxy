#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check

from . import parse_class

class Data5u(precedure.base.Base):

    SOURCE = 'Data5u'
    SEARCH_URL = 'http://www.data5u.com/'
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    # USE_PROXY = True
    MAXPAGE = 1
    POOL_SIZE = 20

    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Data5u add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self):
        page = self.request(
            self.SEARCH_URL,
            timeout=8,
            retry_interval=0,
            retry_time=60,
        )
        return page
        
    def parse(self):
        html = self.download()
        result = []
        bs = bs4.BeautifulSoup(html, 'lxml')
        uls = bs.find_all('ul', class_='l2')
        for ul in uls:
            lis = ul.find_all('li')
            ip = lis[0].text.strip()
            port_cls = lis[1].attrs['class'][-1]
            port = parse_class(port_cls)
            proxy = ':'.join((ip, str(port)))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Data5u().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 35}]

