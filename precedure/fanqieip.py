import lxml.etree

import precedure.base
import utils.proxy_check


class Fanqieip(precedure.base.Base):

    SOURCE = 'Fanqieip'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 100

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(3), merge=True)
        print(('Fanqieip total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Fanqieip add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, pg):
        result = []
        url = f'https://www.fanqieip.com/free/{pg+1}'
        html = self.request(url)
        tree = lxml.etree.HTML(html)
        for tr in tree.xpath('//table[@class="layui-table"]/tbody/tr'):
            ip = tr.xpath('./td[1]/div[1]/text()')[0].strip()
            port = tr.xpath('./td[2]/div[1]/text()')[0].strip()
            proxy = f'{ip}:{port}'
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Fanqieip().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


