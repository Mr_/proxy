#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check

class Freeip(precedure.base.Base):

    SOURCE = 'Freeip'
    SEARCH_URL = 'https://www.freeip.top/?page=%d&anonymity=2'
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    USE_PROXY = True
    MAXPAGE = 1
    POOL_SIZE = 200

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(1, 8), merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Freeip add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, url):
        page = self.request(
            url,
            timeout=11,
            retry_interval=0,
            retry_time=8,
        )
        return page
        
    def parse(self, pg):
        url = self.SEARCH_URL %pg
        html = self.download(url)
        result = []
        bs = bs4.BeautifulSoup(html, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            if len(tds) == 0:
                continue
            ip = tds[0].text.strip()
            port = tds[1].text.strip()
            proxy = ':'.join((ip, str(port)))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Freeip().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()