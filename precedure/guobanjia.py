#coding=utf-8

import re
import bs4
import time

import utils.proxy_check

import precedure.base

class Guobanjia(precedure.base.Base):

    SOURCE = '全网代理IP'
    SEARCH_URL = 'http://www.goubanjia.com'
    INTERVAL = 0
    MAXPAGE = 6
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 150
    # USE_PROXY = True
    MAXPAGE = 1
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Goubanjia add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self):
        page = self.request(
            self.SEARCH_URL,
            timeout=8,
            retry_interval=0,
            retry_time=60,
        )
        return page
        
    def parse(self):
        html = self.download()
        result = []
        bs = bs4.BeautifulSoup(html, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for tr in trs:
            td = tr.find('td')
            if td is None:
                continue
            ip = []
            for item in td.children:
                print((item.name))
                if item.name in ('span', 'div'):
                    ip.append(item.text.strip())
                elif item.name is None:
                    ip.append(':')
            ip = ''.join(ip)
            print(ip)

        divs = bs.find_all('div', class_='dayip')
        ip_re = '\d{0,3}\.\d{0,3}.\d{0,3}.\d{0,3}:\d{0,6}'
        for div in divs:
            link = div.find('a')
            url = link.attrs['href'].strip()
            ip_page = self.request(url)
            ip_strs = re.findall(ip_re + '    &nbsp;&nbsp;.{0,9}    &nbsp;&nbsp;.{0,30}<br>', ip_page)
            for ip_str in ip_strs:
                proxy = re.findall(ip_re, ip_str)[0]
                fetch = {
                'proxy': proxy, 
                'inside': 1 if '中国' in ip_str else 0,
                'sign':proxy,
                }
                fetch.update(self.default_params)
                result.append(fetch)
            time.sleep(2)
        # import ipdb;ipdb.set_trace()
        return result

def schedule_job():
    Guobanjia().run()
    
schedule_plan = [{'trigger': 'date', 'run_date':'3000-01-01'}]

