#coding=utf-8

import json

import utils.proxy_check

import precedure.base

class Horocn(precedure.base.Base):

    SOURCE = '蜻蜓代理'
    SEARCH_URL = 'http://proxy.horocn.com/api/free-proxy?format=json&loc_name=中国&app_id=163578497879151073496'
    MAXPAGE = 1
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 100

    def crawl(self, postdata):
        # import ipdb;ipdb.set_trace()
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Horocn add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self):
        ips = []
        page = self.request(self.SEARCH_URL)
        data = json.loads(page)
        for item in data:
            proxy = '%s:%s' %(item['host'], item['port'])
            fetch = {
                'proxy':proxy, 
                'inside':1,
                'sign':proxy,
                'city': item.get('city_cn', ''),
                'country': item.get('country_cn', ''),
                'region': item.get('province_cn', ''),
                'isp': '',
                }
            fetch.update(self.default_params)
            ips.append(fetch)
        return ips

def schedule_job():
    Horocn().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 31}]


if __name__ == '__main__':
    Horocn().run()
