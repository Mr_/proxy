#coding=utf-8

import bs4

import utils.proxy_check

import precedure.base

class Httpsdaili(precedure.base.Base):

    SOURCE = '瑶瑶代理'
    SEARCH_URL = 'http://www.httpsdaili.com/?page=%d'
    MAXPAGE = 3
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    SIGN = 'sign'
    STORAGE_TABLE = 'proxies'
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        page = self.download(postdata)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Httpsdaili add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, postdata):
        page = self.request(self.SEARCH_URL %postdata[self.PAGE_VAR])
        return page
        
    def parse(self, page):
        result = []
        bs = bs4.BeautifulSoup(page, 'lxml')
        ips = bs.find_all('td', class_='style1')
        ports = bs.find_all('td', class_='style2')
        for ip, port in zip(ips, ports):
            proxy = ':'.join((ip.text.strip(), port.text.strip()))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Httpsdaili().run()
    
schedule_plan = [{'trigger': 'date', 'run_date':'3000-01-01'}]
