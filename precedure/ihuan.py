#coding=utf-8

import re

import bs4

import precedure.base
import utils.proxy_check

class Ihuan(precedure.base.Base):

    SOURCE = '小幻代理'
    BASE_URL = 'https://ip.ihuan.me'
    PRE_URL = 'https://ip.ihuan.me/today.html'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 150

    def crawl(self, postdata):
        html = self.download(self.PRE_URL)
        match = re.findall('/today/\d{4}/\d+/\d+/\d+.html', html)
        urls = [f'{self.BASE_URL}{x}'for x in match]
        result = self.pool.run(self.parse, urls, merge=True)
        result = self.move_same_ip(result)
        print(('Ihuan total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Ihuan add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def move_same_ip(self, result):
        '''
        去重
        '''
        datas, ips = [], set()
        for x in result:
            if x['proxy'] in ips:
                continue
            ips.add(x['proxy'])
            datas.append(x)
        return datas

    def download(self, url, method='GET'):
        page = self.request(
            url,
            method=method,
            timeout=7,
            retry_interval=0,
            retry_time=30,
        )
        return page
        
    def parse(self, url):
        result = []
        html = self.download(url)
        ip_strs = re.findall('\d+\.\d+\.\d+\.\d+:\d+', html)
        for proxy in ip_strs:
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                'city': '',
                'country': '',
                'isp': '',
                'region': '',
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Ihuan().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 180}]

if __name__ == '__main__':
    schedule_job()
