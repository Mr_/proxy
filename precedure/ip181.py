#coding=utf-8

import json

import bs4

import precedure.base
import utils.proxy_check


class Ip181(precedure.base.Base):

    SOURCE = 'Ip181'
    SEARCH_URL = 'http://www.ip181.com/daili/%d.html'
    MAXPAGE = 5
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        page = self.download(postdata)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Ip181 add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, postdata):
        page = self.request(self.SEARCH_URL %postdata[self.PAGE_VAR])
        return page

    def parse(self, page):
        result = []
        proxies = json.loads(page)
        for item in proxies['RESULT']:
            proxy = ':'.join((item['ip'], item['port']))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result
        
def schedule_job():
    Ip181().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]
