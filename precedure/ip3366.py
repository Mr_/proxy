#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check


class Ip3366(precedure.base.Base):

    SOURCE = '云代理'
    SEARCH_URL = 'http://www.ip3366.net/free/?stype=1&page=%d'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        params = (x for x in range(1, 6))
        result = self.pool.run(self.parse, params, merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Ip3366 add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, pg):
        url = self.SEARCH_URL %pg
        return self.request(url)
        
    def parse(self, pg):
        result = []
        html = self.download(pg)
        bs = bs4.BeautifulSoup(html, 'lxml')
        div = bs.find('div', id='list')
        trs = div.find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            if len(tds) == 0 or '.' not in tds[0].text:
                continue
            proxy = f'{tds[0].text.strip()}:{tds[1].text.strip()}'
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Ip3366().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()
