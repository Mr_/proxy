#coding=utf-8

import re

import bs4

import precedure.base
import utils.proxy_check


class Ip66(precedure.base.Base):

    SOURCE = '66ip'
    INTERVAL = 0
    MAXPAGE = 1
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 5
    USE_PROXY = True
    POOL_SIZE = 110
    proxy_num = 300


    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Ip66 add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, num):
        # url = 'http://www.66ip.cn/nmtq.php?'
        # params = {
        #     'getnum': num,
        #     'isp': 0,
        #     'anonymoustype': '4',
        #     'start': '',
        #     'ports': '',
        #     'export': '',
        #     'ipaddress': '',
        #     'area': '1',
        #     'proxytype': '2',
        #     'api': '66ip',
        # }
        # html = self.request(
        #     url,
        #     params=params,
        #     timeout=10,
        #     retry_interval=0,
        #     retry_time=30,
        #     inside='',
        # )
        url = 'http://www.66ip.cn/areaindex_35/index.html'
        html = self.request(
            url,
            timeout=10,
            retry_interval=0,
            retry_time=30,
            inside='',
            byte=True,
        )
        return html

    # def parse(self):
    #     result = []
    #     html = self.download(self.proxy_num)
    #     match = re.findall(r'\d+\.\d+\.\d+\.\d+:\d+', html)
    #     for proxy in match:
    #         if proxy.strip() == '':
    #             continue
    #         fetch = {
    #             'proxy':proxy, 
    #             'inside':1,
    #             'sign':proxy,
    #         }
    #         fetch.update(self.default_params)
    #         result.append(fetch)
    #     return result

    def parse(self):
        result = []
        for x in range(3):
            html = self.download(self.proxy_num)
            bs = bs4.BeautifulSoup(html, 'lxml')
            if bs is not None:
                break
        table = bs.find('div', id='footer').find('table')
        trs = table.find_all('tr')
        skips = set()
        for tr in trs:
            tds = tr.find_all('td')
            ip = tds[0].text.strip()
            if '.' not in ip:
                continue
            port = tds[1].text.strip()
            proxy = f'{ip}:{port}'
            if proxy in skips:
                continue
            skips.add(proxy)
            fetch = {
                'proxy':proxy, 
                'inside':1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Ip66().run()

schedule_plan = [{'trigger': 'interval', 'minutes': 90}]
    
if __name__ == '__main__':
    schedule_job()
