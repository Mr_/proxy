#coding=utf-8
import precedure.base

import re

import utils.proxy_check


class Ip89(precedure.base.Base):

    SOURCE = '89ip'
    SEARCH_URL = 'http://www.89ip.cn/tqdl.html?num=300&address=&kill_address=&port=&kill_port=&isp='
    MAXPAGE = 1
    STORAGE_TABLE = 'proxies'
    USE_PROXY = True
    POOL_SIZE = 200

    def crawl(self, postdata):
        page = self.download(postdata)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Ip89 add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, postdata):
        page = self.request(self.SEARCH_URL)
        return page
        
    def parse(self, page):
        result = []
        ip_re = '\d{0,3}\.\d{0,3}.\d{0,3}.\d{0,3}:\d{0,6}'
        ip_strs = re.findall(ip_re, page)
        for proxy in ip_strs:
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
            }
            fetch.update(self.default_params)
            result.append(fetch)
        # import ipdb;ipdb.set_trace()
        return result

def schedule_job():
    Ip89().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    Ip89().run()
