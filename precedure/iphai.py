#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check

class Iphai(precedure.base.Base):

    SOURCE = 'Iphai'
    SEARCH_URLS = (
            'http://www.iphai.com/free/ng',
            'http://www.iphai.com/free/wg',
    )
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    USE_PROXY = True
    MAXPAGE = 1
    POOL_SIZE = 40

    def crawl(self, postdata):
        result = self.pool.run(self.parse, self.SEARCH_URLS, merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Iphai add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, url):
        page = self.request(
            url,
            timeout=8,
            retry_interval=0,
            retry_time=60,
        )
        return page
        
    def parse(self, url):
        result = []
        html = self.download(url)
        bs = bs4.BeautifulSoup(html, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            if len(tds) == 0:
                continue
            ip = tds[0].text.strip()
            port = tds[1].text.strip()
            proxy = ':'.join((ip, str(port)))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Iphai().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()
