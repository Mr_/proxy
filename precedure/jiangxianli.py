import lxml.etree

import precedure.base
import utils.proxy_check


class Jiangxianli(precedure.base.Base):

    SOURCE = '免费代理库'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 30

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(2), merge=True)
        print(('Jiangxianli total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Jiangxianli add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, page):
        result = []
        url = f'http://ip.jiangxianli.com/?country=中国&page={page+1}'
        html = self.request(url)
        tree = lxml.etree.HTML(html)
        for index, tr in enumerate(tree.xpath("//table//tr")):
            if index == 0:
                continue
            proxy = ':'.join(tr.xpath('./td/text()')[0:2]).strip()
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Jiangxianli().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


