#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check


class Kuaidaili(precedure.base.Base):

    SOURCE = '快代理'
    SEARCH_URL = 'https://www.kuaidaili.com/free/inha/%d/'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 60

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(1, 6), merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Kuaidaili add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, pg):
        result = []
        url = self.SEARCH_URL %pg
        html = self.request(url)
        bs = bs4.BeautifulSoup(html, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for item in trs:
            tds = item.find_all('td')
            if len(tds) == 0 or '.' not in tds[0].text:
                continue
            proxy = ':'.join((tds[0].text.strip(), tds[1].text.strip()))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Kuaidaili().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()
