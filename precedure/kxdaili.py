import precedure.base

import bs4

import utils.proxy_check


class Kxdaili(precedure.base.Base):

    SOURCE = '开心代理'
    SEARCH_URL = 'http://www.kxdaili.com/dailiip/1/%d.html'
    MAXPAGE = 1 
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    SIGN = 'sign'
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(1, 3), merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Kxdaili add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, pg):
        url = self.SEARCH_URL %pg
        page = self.request(url)
        return page

    def parse(self, pg):
        result = []
        html = self.download(pg)
        bs = bs4.BeautifulSoup(html, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for item in trs:
            tds = item.find_all('td')
            if len(tds) <= 1 or '.' not in tds[0].text:
                continue
            proxy = ':'.join((tds[0].text.strip(), tds[1].text.strip()))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Kxdaili().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 30}]

if __name__ == '__main__':
    schedule_job()
