import lxml.etree

import precedure.base
import utils.proxy_check


class Mivip(precedure.base.Base):

    SOURCE = '米扑代理'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 30

    def urls(self):
        urls = (
            # 国内高匿
            'https://proxy.mimvp.com/freeopen',
            # 国外高匿
            'https://proxy.mimvp.com/freeopen?proxy=out_hp'
        )
        return urls

    def crawl(self, postdata):
        result = self.pool.run(self.parse, self.urls(), merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Mivip add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, url):
        page = self.request(url, retry_time=30)
        tree = lxml.etree.HTML(page)
        proxy_list = tree.xpath(".//table[@class='mimvp-tbl free-proxylist-tbl']/tbody/tr")
        result = []
        port_img_map = {'DMxMjg': '3128', 'Dgw': '80', 'DgwODA': '8080',
            'DgwOA': '808', 'DgwMDA': '8000', 'Dg4ODg': '8888', 'DgxMjM': '8123',
            'DgwODE': '8081', 'Dk5OTk': '9999', 'DU1NDQz': '55443',}
        for tr in proxy_list:
            try:
                ip = ''.join(tr.xpath('./td[2]/text()'))
                port_img = ''.join(tr.xpath('./td[3]/img/@src')).split("port=")[-1]
                print(ip, port_img[14:].replace('O0O', ''))
                port = port_img_map.get(port_img[14:].replace('O0O', ''))
                if port:
                    proxy = f'{ip}:{port}'
                    fetch = {
                        'proxy': proxy, 
                        'inside': 1,
                        'sign': proxy,
                        'country': '',
                        'region': '',
                        'city': '',
                        'isp': '',
                        }
                    fetch.update(self.default_params)
                    result.append(fetch)
            except Exception as e:
                print(e)
        return result

def schedule_job():
    Mivip().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


