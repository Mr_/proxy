#coding=utf-8

import json

import precedure.base
import utils.proxy_check


class Mogumiao(precedure.base.Base):

    SOURCE = '蘑菇代理'
    SEARCH_URLS = (
        'http://www.moguproxy.com/proxy/free/listFreeIp',
    )
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        result = []
        for url in self.SEARCH_URLS:
            page = self.request(url)
            result.extend(self.parse(page))
        result = utils.proxy_check.checks(self.pool, result, 'Mogumiao add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, page):
        result = []
        json_obj = json.loads(page)
        for item in json_obj['msg']:
            proxy = ':'.join((item['ip'], str(item['port'])))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Mogumiao().run()
    
schedule_plan = [{'trigger': 'date', 'run_date':'3000-01-01'}]
