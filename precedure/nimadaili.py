#coding=utf-8

import re

import precedure.base
import utils.proxy_check


class Nimadaili(precedure.base.Base):

    SOURCE = 'Nimadaili'
    INTERVAL = 0
    MAXPAGE = 1
    STORAGE_TABLE = 'proxies'
    USE_PROXY = True
    POOL_SIZE = 110
    proxy_num = 300
    BASE_URL = 'http://www.nimadaili.com/gaoni/%d/'


    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(1, 11), merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Nimadaili add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, num):
        url = self.BASE_URL %num
        page = self.request(
            url,
            timeout=10,
            retry_interval=0,
            retry_time=30,
            inside='',
        )
        return page

    def parse(self, num):
        result = []
        html = self.download(num)
        match = re.findall(r'\d+\.\d+\.\d+\.\d+:\d+', html)
        for proxy in match:
            if proxy.strip() == '':
                continue
            fetch = {
                'proxy':proxy, 
                'inside':1,
                'sign':proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Nimadaili().run()

schedule_plan = [{'trigger': 'interval', 'minutes': 60}]
    
if __name__ == '__main__':
    schedule_job()
