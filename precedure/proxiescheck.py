#coding=utf-8

import datetime

import precedure.base
import utils.proxy_check


class Proxiescheck(precedure.base.Base):

    INTERVAL = 2
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    POOL_LIMIT = 500
    POOL_SIZE = POOL_LIMIT

    def run(self):
        utils.proxy_check.init_local_ip()
        self.update({})
        tb = self.STORAGE_TABLE
        sql = f'insert ignore into proxy_history(proxy) select proxy from {tb} where del<=-1'
        self.storage.excute_sql(sql)
        sql = f'delete from {tb} where del<=-1;'
        self.storage.excute_sql(sql)
        sql = ' '.join((
            'delete from %s' %self.STORAGE_TABLE,
            'where http!=1 and https!=1',
        ))
        self.storage.excute_sql(sql)

    def get_proxys(self, page, times=4):
        page_limit = times * self.POOL_LIMIT
        sql = ' '.join((
            f'select * from {self.STORAGE_TABLE} order by id ',
            f'limit {(page - 1) * page_limit}, {page_limit}',
        ))
        return self.storage.excute_sql(sql)

    def cmp_new_pos(self, olds, news):
        '''
        对比新老数据，找出新爬取到位置信息的数据
        '''
        result = {}
        none_id = set(x['id'] for x in olds if x['country'] == '')
        for data in news:
            if data['id'] in none_id:
                country = data.get('country', None)
                if country is not None and country != '':
                    result[data['id']] = data
        return result

    def up(self, proxys):
        result = utils.proxy_check.checks(self.pool, proxys, 'Proxy check :')
        utils.proxy_check.check_pos(self.pool, result)
        pos = self.cmp_new_pos(proxys, result)
        result = {x['id']: x for x in result}
        ups = []
        for item in proxys:
            ip_id = item['id']
            up_data = dict(id=ip_id)
            if ip_id in result:
                up_data['del'] = item['del'] + 1 if item['del'] <= 5 else item['del']
                up_data['http'] = result[ip_id]['http']
                up_data['https'] = result[ip_id]['https']
            else:
                up_data['del'] = item['del'] - 1 if item['del'] >= 0 else item['del']
            if ip_id in pos:
                up_pos = pos[ip_id]
                up_data['country'] = up_pos['country']
                up_data['region'] = up_pos['region']
                up_data['city'] = up_pos['city']
                up_data['isp'] = up_pos['isp']
            ups.append(up_data)
        return ups

    def crawl(self, postdata):
        proxys = self.get_proxys(postdata[self.PAGE_VAR])
        return self.up(proxys)

    def save(self, data_list):
        self.storage.update_data(self.STORAGE_TABLE, data_list, 'id')

def schedule_job():
    Proxiescheck().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 15}]

if __name__ == '__main__':
    # p = Proxiescheck()
    # sql = "SELECT * FROM info.proxies where proxy like '190.108.252.109%'"
    # sql = "SELECT * FROM info.proxies where proxy limit 300"
    # datas = p.storage.excute_sql(sql)
    # p.up(datas)
    schedule_job()
    import ipdb;ipdb.set_trace()
    pass