import lxml.etree

import precedure.base
import utils.proxy_check


class Proxydb(precedure.base.Base):

    SOURCE = 'Proxydb'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 30

    def crawl(self, postdata):
        result = self.parse()
        print(('Proxydb total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Proxydb add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self):
        result = []
        url = 'http://proxydb.net/?protocol=https&country=CN'
        html = self.request(url)
        tree = lxml.etree.HTML(html)
        for tr in tree.xpath("//table/tbody/tr"):
            proxy = tr.xpath('./td/a[1]/text()')[0].strip()
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Proxydb().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


