import re
import json
import lxml.etree

import precedure.base
import utils.proxy_check


class Pzzqz(precedure.base.Base):

    SOURCE = 'Pzzqz代理'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    USE_SESSION = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 30

    def crawl(self, postdata):
        result = self.parse()
        print(('Pzzqz total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Pzzqz add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self):
        url = 'https://pzzqz.com'
        result = []
        index_html = self.request(url, timeout=20)
        tree = lxml.etree.HTML(index_html)
        trs = tree.xpath('.//table[@class="table table-hover"]/tbody/tr')
        x_csrf_token = re.findall('X-CSRFToken": "(.*?)"', index_html)
        if len(x_csrf_token) > 0:
            data = {"http": "on", "ping": "3000", "country": "cn", "ports": ""}
            head = {
                'X-CSRFToken': x_csrf_token[0],
                'content-type': 'application/json;charset=UTF-8'}
            html = self.request(url, method='POST', header=head, params=data)
            resp = json.loads(html)
            tree = lxml.etree.HTML(resp['proxy_html'])
            trs.extend(tree.xpath("//tr"))
        for tr in trs:
            ip = "".join(tr.xpath("./td[1]/text()"))
            port = "".join(tr.xpath("./td[2]/text()"))
            proxy = f'{ip}:{port}'
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Pzzqz().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


