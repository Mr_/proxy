import bs4

import precedure.base
import utils.proxy_check


class Qydaili(precedure.base.Base):

    SOURCE = '旗云代理'
    MAXPAGE = 1
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 10
    URL = 'http://www.qydaili.com/free/'

    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Qydaili add : ')
        print(f'Qydaili real proxy {len(result)}')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self):
        html = self.request(self.URL)
        bs = bs4.BeautifulSoup(html, 'lxml')
        tb = bs.find('table')
        trs = tb.find_all('tr')
        result = []
        for tr in trs:
            tds = tr.find_all('td')
            if len(tds) == 0:
                continue
            proxy = f'{tds[0].text.strip()}:{tds[1].text.strip()}'
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Qydaili().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 360}]

if __name__ == '__main__':
    schedule_job()


