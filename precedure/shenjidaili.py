import lxml.etree

import precedure.base
import utils.proxy_check


class Shenjidaili(precedure.base.Base):

    SOURCE = 'shenjidaili'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 30

    def crawl(self, postdata):
        result = self.parse()
        print(('Shenjidaili total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Shenjidaili add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self):
        url = 'http://www.shenjidaili.com/product/open/'
        result = []
        html = self.request(url, timeout=20)
        tree = lxml.etree.HTML(html)
        for table in tree.xpath("//table[@class='table table-hover text-white text-center table-borderless']"):
            for tr in table.xpath("./tr")[1:]:
                proxy = ''.join(tr.xpath("./td[1]/text()"))
                fetch = {
                    'proxy': proxy, 
                    'inside': 1,
                    'sign': proxy,
                    'country': '',
                    'region': '',
                    'city': '',
                    'isp': '',
                    }
                fetch.update(self.default_params)
                result.append(fetch)
        return result

def schedule_job():
    Shenjidaili().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


