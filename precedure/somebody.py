#coding=utf-8

import re

import utils.builtin
import precedure.base
import utils.proxy_check


class Somebody(precedure.base.Base):

    SOURCE = '无域名网站'
    SEARCH_URL = 'http://47.97.7.119:8080/proxypool/proxyController/queryProxy?'
    USE_BROWSER = False
    USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20
    
    def crawl(self, postdata):
        page = self.download(postdata)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Somebody add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, postdata):
        params = {
            'page': postdata[self.PAGE_VAR],
            'rows': 45,
            '_': utils.builtin.timestamp(),
        }
        page = self.request(self.SEARCH_URL, params=params)
        return page

    def parse(self, page):
        result = []
        ip_re = '\d{0,3}\.\d{0,3}.\d{0,3}.\d{0,3}:\d{0,6}'
        ip_strs = re.findall(ip_re, page)
        for proxy in ip_strs:
            fetch = {
            'proxy': proxy, 
            'inside': 1,
            'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Somebody().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 30}]
