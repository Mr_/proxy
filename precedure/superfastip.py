import json

import precedure.base
import utils.proxy_check


class Superfastip(precedure.base.Base):

    SOURCE = '极速代理'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 30

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(2), merge=True)
        print(('Superfastip total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Superfastip add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, page):
        url = f'https://api.superfastip.com/ip/freeip?page={page+1}'
        result = []
        html = self.request(url)
        resp = json.loads(html)
        for each in resp.get("freeips", []):
            proxy = '%s:%s' %(each.get('ip'), each.get('port'))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Superfastip().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


