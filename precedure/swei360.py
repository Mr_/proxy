#coding=utf-8

import bs4

import utils.proxy_check

import precedure.base

class Swei360(precedure.base.Base):

    SOURCE = 'Swei360'
    SEARCH_URL = 'http://www.swei360.com/free/?stype=1&page=%d'
    MAXPAGE = 5
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 15

    def crawl(self, postdata):
        page = self.download(postdata)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Swei360 add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, postdata):
        page = self.request(self.SEARCH_URL %postdata[self.PAGE_VAR])
        return page
        
    def parse(self, page):
        result = []
        bs = bs4.BeautifulSoup(page, 'lxml')
        table = bs.find('table')
        trs = table.find_all('tr')
        for item in trs:
            tds = item.find_all('td')
            if len(tds) <= 1 or '.' not in tds[0].text:
                continue
            proxy = ':'.join((tds[0].text.strip(), tds[1].text.strip()))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Swei360().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 30}]
