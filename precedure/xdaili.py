#coding=utf-8

import json

import utils.proxy_check

import precedure.base

class Xdaili(precedure.base.Base):

    """
    抓取讯代理免费proxy http://www.xdaili.cn/ipagent/freeip/getFreeIps?page=1&rows=10
    :return:
    """
    
    SOURCE = '讯代理'
    INTERVAL = 0
    MAXPAGE = 1
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 5
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20
    proxy_num = 200

    def crawl(self, postdata):
        page = self.download(self.proxy_num)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Xdaili add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self, num):
        url = 'http://www.xdaili.cn/ipagent/freeip/getFreeIps?page=1&rows=10'
        page = self.request(url)
        return page

    def parse(self, page):
        result = []
        res = json.loads(page)
        try:
            for row in res['RESULT']['rows']:
                proxy = '{}:{}'.format(row['ip'], row['port'])
                fetch = {
                    'proxy':proxy, 
                    'inside':1,
                    'sign':proxy,
                }
                fetch.update(self.default_params)
                result.append(fetch)
        except Exception as e:
            pass
        return result

def schedule_job():
    Xdaili().run()
