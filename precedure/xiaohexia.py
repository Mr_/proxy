#coding=utf-8

import json

import precedure.base
import utils.proxy_check

class Xiaohexia(precedure.base.Base):

    SOURCE = '小河虾'
    SEARCH_URL = 'http://www.xiaohexia.cn/getip.php?num=100&format=json&protocol=https'
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    USE_PROXY = True
    MAXPAGE = 1
    # FILTER_KEY = 'active_proxies'
    POOL_SIZE = 20

    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Xiaohexia add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self):
        page = self.request(
            self.SEARCH_URL,
            timeout=8,
            retry_interval=0,
            retry_time=60,
        )
        return page
        
    def parse(self):
        result = []
        html = self.download()
        resp = json.loads(html)
        for proxy in resp:
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Xiaohexia().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]
