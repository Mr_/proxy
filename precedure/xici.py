#coding=utf-8
import precedure.base

import time
import lxml.etree

import utils.proxy_check


class Xici(precedure.base.Base):

    SOURCE = '西刺代理'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 300

    def urls(self):
        urls = (
            # 国内高匿
            'http://www.xicidaili.com/nn/{}',
            # 国内普通
            'http://www.xicidaili.com/nt/{}',
            # 国内HTTP
            'http://www.xicidaili.com/wn/{}',
            # 国内HTTPS
            'http://www.xicidaili.com/wt/{}',
        )
        return urls

    def crawl(self, postdata):
        def crawl_page(data):
            result.extend(self.parse(data))

        result = []
        params = []
        for url in self.urls():
            params.extend([{'url': url, 'page': x}for x in range(1, 11)])
        self.pool.run(crawl_page, params)
        print(('xici total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Xici add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, postdata):
        url = postdata['url'].format(postdata['page'])
        page = self.request(url, retry_time=30)
        tree = lxml.etree.HTML(page)
        proxy_list = tree.xpath('.//table[@id="ip_list"]//tr')
        result = []
        for proxy in proxy_list:
            proxy = ':'.join(proxy.xpath('./td/text()')[0:2])
            if proxy.strip() == '':
                continue
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Xici().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


