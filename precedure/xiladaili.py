import re

import precedure.base
import utils.proxy_check


class Xiladaili(precedure.base.Base):

    SOURCE = '西拉代理'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    PUSH_NUM = 1
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 100

    def crawl(self, postdata):
        result = self.pool.run(self.parse, range(4), merge=True)
        print(('Xiladaili total num:{}'.format(len(result))))
        result = utils.proxy_check.checks(self.pool, result, 'Xiladaili add : ')
        print(('real proxy %d' %len(result)))
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, page):
        result = []
        url = f'http://www.xiladaili.com/https/{page+1}/'
        html = self.request(url)
        ips = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}", html)
        for ip in ips:
            proxy = ip.strip()
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign': proxy,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Xiladaili().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]

if __name__ == '__main__':
    schedule_job()


