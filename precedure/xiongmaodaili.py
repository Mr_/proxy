#coding=utf-8

import json

import precedure.base
import utils.proxy_check


class Xiongmaodaili(precedure.base.Base):

    SOURCE = '熊猫代理'
    SEARCH_URL = 'http://www.xiongmaodaili.com/xiongmao-web/freeip/list'
    MAXPAGE = 1
    USE_BROWSER = False
    USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 10

    def crawl(self, postdata):
        page = self.request(self.SEARCH_URL)
        result = self.parse(page)
        result = utils.proxy_check.checks(self.pool, result, 'Xiongmaodaili add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, page):
        result = []
        json_obj = json.loads(page)
        for item in json_obj['obj']:
            proxy = ':'.join((item['ip'], str(item['port'])))
            fetch = {
                'proxy': proxy, 
                'inside': 1,
                'sign':proxy,
                }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Xiongmaodaili().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 10}]
