#coding=utf-8

import re
import json

import bs4

import precedure.base
import utils.proxy_check


class Xsdl(precedure.base.Base):

    """
    小舒代理 http://www.xsdaili.com/
    :return:
    """
    
    BASE_URL = 'http://www.xsdaili.com'
    SOURCE = '小舒代理'
    INTERVAL = 0
    MAXPAGE = 1
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 5
    USE_PROXY = True
    POOL_SIZE = 110

    def get_urls(self):
        page = self.request(self.BASE_URL)
        bs = bs4.BeautifulSoup(page, 'lxml')
        items = bs.find_all('div', class_='title')
        urls = []
        for item in items[:10]:
            link = item.find('a')
            url = link.attrs['href'].strip()
            url = ''.join((self.BASE_URL, url))
            urls.append(url)
        return urls

    def crawl(self, postdata):
        urls = self.get_urls()
        result = self.pool.run(self.parse, urls, merge=True)
        result = utils.proxy_check.checks(self.pool, result, 'Xsdl add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, url):
        page = self.request(url)
        result = []
        ip_re = '\d{0,3}\.\d{0,3}.\d{0,3}.\d{0,3}:\d{0,6}'
        items = re.findall(ip_re, page)
        for item in items:
            fetch = {
                'proxy': item,
                'inside': 1,
                'sign': item,
                'country': '',
                'region': '',
                'city': '',
                'isp': '',
            }
            fetch.update(self.default_params)
            result.append(fetch)
        return result

def schedule_job():
    Xsdl().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60 * 4}]

if __name__ == '__main__':
    schedule_job()
