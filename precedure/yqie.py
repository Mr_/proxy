#coding=utf-8

import bs4

import precedure.base
import utils.proxy_check

class Yqie(precedure.base.Base):

    SOURCE = 'Yqie'
    SEARCH_URL = 'http://ip.yqie.com/ipproxy.htm'
    USE_BROWSER = False
    STORAGE_TABLE = 'proxies'
    PUSH_NUM = 1
    USE_PROXY = True
    MAXPAGE = 1
    POOL_SIZE = 25

    def crawl(self, postdata):
        result = self.parse()
        result = utils.proxy_check.checks(self.pool, result, 'Yqie add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def download(self):
        page = self.request(
            self.SEARCH_URL,
            timeout=8,
            retry_interval=0,
            retry_time=60,
        )
        return page
        
    def parse(self):
        html = self.download()
        result = []
        bs = bs4.BeautifulSoup(html, 'lxml')
        tables = bs.find_all('table', id='GridViewOrder')
        for table in (tables[0], tables[2]):
            trs = table.find_all('tr')
            for tr in trs:
                tds = tr.find_all('td')
                if len(tds) == 0:
                    continue
                ip = tds[0].text.strip()
                port = tds[1].text.strip()
                proxy = ':'.join((ip, str(port)))
                fetch = {
                    'proxy': proxy, 
                    'inside': 1,
                    'sign':proxy,
                }
                fetch.update(self.default_params)
                result.append(fetch)
        return result

def schedule_job():
    Yqie().run()
    
schedule_plan = [{'trigger': 'interval', 'minutes': 60}]
