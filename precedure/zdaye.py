#coding=utf-8

import re

import utils.proxy_check

import precedure.base

class Zdaye(precedure.base.Base):

    SOURCE = '站大爷'
    BASE_URL = 'http://ip.zdaye.com'
    SEARCH_URL = 'http://ip.zdaye.com/dayProxy.html'
    MAXPAGE = 1
    USE_BROWSER = False
    # USE_PROXY = True
    STORAGE_TABLE = 'proxies'
    POOL_SIZE = 100
    # POOL_SIZE = 1

    def crawl(self, postdata):
        # import ipdb;ipdb.set_trace()
        page = self.request(self.SEARCH_URL)
        urls = re.findall('<a href="(/dayProxy/ip/\d+.html)">\d{4}', page)
        urls = (''.join((self.BASE_URL, x))for x in urls[:1])
        result = self.parse(urls)
        result = utils.proxy_check.checks(self.pool, result, 'Zdaye add : ')
        utils.proxy_check.check_pos(self.pool, result)
        return result

    def parse(self, urls):
        def crawl_pg(url):
            ips = []
            page = self.request(
                url,
                header={'Referer':'http://ip.zdaye.com/dayProxy.html'}
            )
            ip_re = '\d{0,3}\.\d{0,3}.\d{0,3}.\d{0,3}:\d{0,6}'
            match = re.findall(ip_re, page)
            # import ipdb;ipdb.set_trace()
            if len(match) == 0:
                print(page)
            for proxy in match:
                if proxy.strip() == '':
                    continue
                fetch = {
                    'proxy':proxy, 
                    'inside':1,
                    'sign':proxy,
                    'city': '',
                    'country': '',
                    'region': '',
                    'isp': '',
                    }
                fetch.update(self.default_params)
                ips.append(fetch)
            print(('Crawl %s' %url))
            return ips
        result = self.pool.run(crawl_pg, urls, merge=True)
        return result

def schedule_job():
    Zdaye().run()
    
schedule_plan = [{'trigger': 'date', 'run_date':'3000-01-01'}]

if __name__ == '__main__':
    Zdaye().run()
