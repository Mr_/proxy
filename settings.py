# encoding: utf-8

STORAGE_PATH = 'output'

CHROMEDRIVER_PATH = '/usr/bin/chromedriver'
PLANTHONJS_PATH = ''

SCH_LOG_PATH = 'logs'
SCH_LOG_FILE = 'schdeduler.db'
SCH_JOBS = []
SCH_JOB_PATH = 'precedure'
SCH_JOB_EXCLUDES = ['base.py']

SCHEDULER_EXECUTORS = {
    'default': ('ThreadPoolExecutor', 30),
    'processpool': ('ProcessPoolExecutor', 4),
}

SCH_DEF_PLAN = [{'trigger': 'interval', 'minutes':17,}]

DATABASE_CONFIG = {
    'port':  3306,
    'host': 'localhost',
    # 'user':  'root',
    # 'password': 'guest123',
    # 'host': '120.132.56.91',
    'user':  'info',
    'password': 'XhAIjHgAUGMYrNvG',
    'db': 'proxy',
    'charset': 'utf8',
}

DATABASE_CONFIG = {
    'port':  3306,
    'host': 'localhost',
    'user':  'root',
    'password': 'root123456',
    'db': 'proxy',
    'charset': 'utf8mb4',
}


REDIS_CONFIG = {
    'host':'localhost',
    'port':6379,
    'db':0,
}

BOSS_KEY = 'ijfn@9!?jd'

MOST_LABEL_NUM = 8

PROPULAR_WORDS_NUM = 5

PROXY_BASE = 'http://localhost:6001/'
# PROXY_BASE = 'http://120.132.56.91:6001/'
PROXY_GET = ''.join((PROXY_BASE, 'get?http={http}&https={https}&inside={inside}&size={size}'))
PROXY_DEL = ''.join((PROXY_BASE, 'delete/?proxy={}'))