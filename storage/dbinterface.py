#coding=utf-8

import utils.builtin
import storage.base_db

from settings import DATABASE_CONFIG


class DBInterface(object):

    def __init__(self, db_setting=None):
        if db_setting is None:
            db_setting = DATABASE_CONFIG
        self.db = storage.base_db.Dbalchemy(**db_setting)

    def push_data(self, storage_type, datas):
        '''
        往指定表添加数据
        param：storage_type，表名
        param：datas，数据
        ''' 
        conn = self.db.connect()
        try:
            size = 200
            total = len(datas)
            if total % size == 0:
                max_pg = total // size
            else:
                max_pg = (total // size) + 1
            for pg in range(max_pg):
                start = pg * size
                end = (pg + 1) * size
                params = {
                    'operator': 'ignore',
                    'table': storage_type,
                    'values': datas[start:end],
                }
                if self.db.is_sqllite:
                    params['operator'] = 'insert'
                sql = self.db.get_insert_sql(params)
                try:
                    self.db.excute_sql_once(conn, sql)
                except Exception as e:
                    print(sql)
                    raise e
        finally:
            conn.close()
            
    def update_data(self, storage_type, datas, key='id'):
        conn = self.db.connect()
        try:
            for _data in datas:
                params = {
                    'update': storage_type,
                    'set': _data,
                    'where':[
                        (key, 'equals', str(_data.pop(key))),
                    ],
                }
                sql = self.db.get_sql(params)
                try:
                    self.db.excute_sql_once(conn, sql)
                except Exception as e:
                    print(sql)
                    raise e
        finally:
            conn.close()

    def excute_sql(self, sql):
        '''
        执行一次SQL
        param:sql,SQL语句
        '''
        return self.db.excute_sql(sql)

    def sign_length(self, storage_type, sign='sign'):
        '''
        获取指定表sign字段的数量
        param：storage_type，表名
        '''
        params = {
            'select': ['count(%s)' %sign],
            'from': [storage_type],
        }
        sql = self.db.get_sql(params)
        data = self.db.excute_sql(sql)
        return data[0][0]

    def get_signs_in_set(self, storage_type, sign='sign'):
        '''
        获取指定表所有sign字段内容
        param：storage_type，表名
        '''
        result = set()
        limit = 100000
        for pg in range(99999):
            sql = ' '.join((
                'select %s' %sign,
                'from %s' %storage_type,
                'order by id',
                'limit %d, %d' %(pg * limit, limit),
            ))
            datas = self.db.excute_sql(sql)
            if len(datas) == 0:
                break
            for data in datas:
                result.add(data[sign])
        return result

    def clean_table(self, table_name):
        self.excute_sql('TRUNCATE %s' %table_name)

    def rename(self, old_name, new_name):
        '''
        重命名
        '''
        sql = '\n'.join((
            'ALTER TABLE `%s`' %old_name,
            'RENAME TO `%s`;' %new_name,
        ))
        self.excute_sql(sql)

    def exist_table(self, table_name):
        '''
        检查表是否存在
        '''
        sql = f'show tables like "{table_name}"'
        return True if len(self.excute_sql(sql)) > 0 else False

    def get_column_names(self, table_name):
        '''
        获取数据表的所有字段名称
        '''
        sql = ' '.join((
            'select column_name from information_schema.columns',
            f'where table_name="{table_name}"',
        ))
        return [x['column_name']for x in self.excute_sql(sql)]

    def get_same_keys(self, keys, table):
        '''
        将两份字段列表进行对比，找出相同的字段名称
        params:
            keys(list) 第一份字段名列表
            table(str) 数据表名称，用来确定目标表
        '''
        db_keys = self.get_column_names(table)
        return set(keys) & set(db_keys)
        
    def close(self):
        pass

CLS_NAME = DBInterface
