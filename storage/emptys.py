import json
import os.path

from storage.titles import Titles


class Emptys(Titles):
    path = 'EMPTYS'

    def add_datas(self, classify_id, md5, except_data, committer=None):
        if self.exists(classify_id, md5):
            return False
        exist_datas = self.table[classify_id]
        exist_datas.update({md5: except_data})
        filename = classify_id + '.json'
        dump_data = json.dumps(exist_datas, sort_keys=True, indent=4, ensure_ascii=False)
        self.interface.modify_file(
            os.path.join(self.path, filename), 
            dump_data.encode('utf8'),
            message="Add to classify id :" + filename,
            committer=committer)
        return True
