import glob
import os.path

import utils.builtin

from settings import STORAGE_PATH


class FSInterface(object):
    FILE_EXT = 'json'
    FOLDER_SIZE = 2000

    def __init__(self, path):
        self.path = os.path.join(STORAGE_PATH, path)
        self._save_path = None
        self._save_paths = []

    def initialize(self, storage_type, batching=False):
        self.batching = batching
        self._save_path = os.path.join(self.path, storage_type)
        if not os.path.exists(self._save_path):
            os.makedirs(self._save_path)
        if self.batching:
            dirs = os.listdir(self._save_path)
            for _dir in dirs:
                _sub_path = os.path.join(self._save_path, _dir)
                if os.path.isdir(_sub_path):
                    self._save_paths.append(_sub_path)
            if len(self._save_paths) == 0:
                new_sub_path = os.path.join(self._save_path, '1')
                os.makedirs(new_sub_path)
                self._save_paths.append(new_sub_path)
            else:
                self._save_paths.sort(lambda f1,f2:cmp(int(os.path.basename(f1)), int(os.path.basename(f2))))
        else:
            self._save_paths.append(self._save_path)

    def get_data(self, classify_id, only_ids=True):
        filename = os.path.extsep.join((classify_id, self.FILE_EXT))
        if self.batching:
            for _path in self._save_paths:
                if os.path.exists(os.path.join(_path, filename)):
                    data = utils.builtin.load_json(_path, filename)
                    break
            else:
                data = {}
        else:
            if not os.path.exists(os.path.join(self.save_path, filename)):
                self.update_data(classify_id, {}, "Add classify file: " + filename)
                return {}
            data = utils.builtin.load_json(self.save_path, filename)
            if only_ids:
                re_data = {}
                for k, v in list(data.items()):
                    re_data[k] = {'md5': v['md5']}
                data = re_data
        return data

    def update_data(self, classify_id, data, *args, **kwargs):
        filename = os.path.extsep.join((classify_id, self.FILE_EXT))
        if not self.batching:
            if len(data) > 0:
                table = self.get_data(classify_id, only_ids=False)
                data.update(table)
        utils.builtin.save_json(data, self.save_path, filename)
        return True

    @property
    def save_path(self):
        if self.batching:
            _file = os.path.extsep.join(('*', self.FILE_EXT))
            files = glob.glob(os.path.join(self._save_paths[-1], _file))
            if len(files) >= self.FOLDER_SIZE:
                _name = os.path.basename(self._save_paths[-1])
                _new_name = str(int(_name) + 1)
                _path = os.path.join(self._save_path, _new_name)
                os.makedirs(_path)
                self._save_paths.append(_path)
        return self._save_paths[-1]

    def close(self):
        self._save_path = None
        self._save_paths = []
