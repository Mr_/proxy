#coding=utf8

from redis import Redis
from redis.connection import BlockingConnectionPool


class Redis_client(object):

    def __init__(self, **kwargs):
        self.db = int(kwargs.get('db', 0))
        self.host = kwargs.get('host', 'localhost')
        self.port = int(kwargs.get('port', 6379))
        self.conn = self.connect()

    def connect(self):
        connection = Redis(connection_pool=BlockingConnectionPool(
                    host=self.host, 
                    port=self.port,
                    db=self.db,
                    decode_responses=True,))
        return connection

    def sadd(self, key, values):
        '''
        集合操作，给集合key添加数据values
        '''
        if type(values) not in (list, set, tuple):
            values = (values,)
        pipe = self.conn.pipeline()
        pipe.sadd(key, *values)
        pipe.execute()
        return True

    def sismember(self, key, value):
        '''
        key中是否存在value
        '''
        return self.conn.sismember(key, value)

    def smembers(self, key):
        '''
        返回集合 key 中的所有成员
        '''
        return self.conn.smembers(key)

    def scard(self, key):
        '''
        返回集合 key 的基数(集合中元素的数量)
        '''
        return self.conn.scard(key)

    def delete(self, key):
        '''
        删除某个key
        '''
        return self.conn.delete(key)

    def pop(self, key):
        '''
        从key的集合的右侧移除一个元素，并将其返回
        '''
        return self.conn.spop(key)

    def rpush(self, key, values):
        '''
        列表操作，从列表key尾部添加数据values
        '''
        if type(values) not in (list, set, tuple):
            values = (values,)
        if len(values) > 0:
            pipe = self.conn.pipeline()
            pipe.rpush(key, *values)
            pipe.execute()
        return True

    def lpush(self, key, values):
        '''
        列表操作，从列表key头部添加数据values
        '''
        if type(values) not in (list, set, tuple):
            values = (values,)
        pipe = self.conn.pipeline()
        pipe.lpush(key, *values)
        pipe.execute()
        return True

    def llen(self, key):
        '''
        返回列表长度
        '''
        return self.conn.llen(key)

    def lget(self, key, num):
        '''
        列表取值
        '''
        pipe = self.conn.pipeline()
        total = self.llen(key)
        if total < num:
            num = total
        [pipe.lpop(key) for x in range(num)]
        return pipe.execute()

    def lrange(self, key, start, end):
        '''
        返回列表数据
        '''
        return self.conn.lrange(key, start, end)

    def hmset(self, key, datas):
        '''
        hash操作，批量set数据
        params: data(dict)
        '''
        pipe = self.conn.pipeline()
        pipe.hmset(key, datas)
        pipe.execute()
        return True

    def hgetall(self, key):
        '''
        hash操作，取出所有键值对
        '''
        return self.conn.hgetall(key)

    def hincrby(self, table, key, amount=1):
        '''
        hash操作，取出所有键值对
        '''
        return self.conn.hincrby(table, key, amount)
