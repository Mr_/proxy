#coding=utf8

class Xlshelper(object):
    """docstring for XlsHelper"""
    def __init__(self, fileName):
        super(Xlshelper, self).__init__()
        self.fileName = fileName

    def create(self, datas):
        import xlwt
        workbook = xlwt.Workbook(encoding='utf-8')

        for _datas in datas:
            sheetName = _datas['name']
            data = _datas['data']
            booksheet = workbook.add_sheet(sheetName, cell_overwrite_ok=True)

            for i, row in enumerate(data):
                for j, col in enumerate(row):
                    booksheet.write(i, j, col)

        workbook.save(self.fileName)

        print(('Save %s success ...' %self.fileName))

    def read(self, index = None, tableName = None):
        import xlrd
        data = xlrd.open_workbook(self.fileName)
        if not index is None:
            return data.sheet_by_index(index)
        
        if not tableName is None:
            return data.sheet_by_name(tableName)

        return data

    # 读取工作表
    def reads(self, indexs = None):
        import xlrd
        tables = []
        data = xlrd.open_workbook(self.fileName)
        if indexs is None:
            # indexs为空时获取所有工作表的数据
            tableCount = len(data.sheets())
            for _index in range(0,tableCount):
                tables.append(data.sheet_by_index(_index))
        else:
            if not isinstance(indexs, list):
                print('XlsHelper reads error:indexs is not a list')
                return
            # 获取指定工作表的数据
            for _index in indexs:
                if isinstance(_index, str):
                    # 字符串认为是名字
                    tables.append(data.sheet_by_name(_index))
                else:
                    tables.append(data.sheet_by_index(_index))
        return tables


def save(name, datas):
    Xlshelper(name).create(datas)