import json

import net.request


class Bd_pos(object):
    '''
    百度获取经纬度接口
    '''

    def __init__(self):
        self.AKS = [
            'AXjBDNBMmwZ6BsBPavPg8wq7GNXvoY2Z',
            'Edahle1G6Sfrl9FIBDadqwRGggZo0W5n',
            'xMZeGOkl7wW1c75kD9bWPDtT1TKpLTr5',
        ]
        self.dri = net.request.Request()

    def get(self, address, city):
        '''
        根据城市和地址获取经纬度
        '''
        while True:
            ak = self.AKS[0]
            base_url = 'http://api.map.baidu.com/geocoding/v3'
            args = u'/?address=%s&output=json&ak=%s&city=%s市'%(
                address,
                ak, 
                city)
            url = u'%s%s'%(base_url, args)
            html = self.dri.req(url)
            data = json.loads(html)
            status = data['status']
            if status == 0:
                res = data['result']
                info = res['location']
                info['precise'] = res['precise']
                info['confidence'] = res['confidence']
                info['comprehension'] = res['comprehension']
                return info
            elif status == 302:
                if len(self.AKS) > 1:
                    self.AKS.pop(0)
                    continue
                else:
                    return None
            else:
                info = {
                    'lat': 0,
                    'lng': 0,
                }
            return info