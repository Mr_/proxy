import json

import storage.dbinterface
import storage.redis_client

class Box(object):
    '''
    用于分布式的数据工具
    '''

    def __init__(self, key):
        self.redis = storage.redis_client.Redis_client()
        self.db = storage.dbinterface.DBInterface()
        self.key = key
        self.push_sql = None
        self.push_without_id_sql = None

    def push(self, ids=None):
        '''
        往redis里添加待用数据
        '''
        sql = self.push_sql
        if ids is not None and len(ids) > 0:
            ids_desc = ','.join(ids)
            sql = self.push_without_id_sql %ids_desc
        res = self.db.excute_sql(sql)
        datas = []
        for x in res:
            datas.append(json.dumps(dict(x), ensure_ascii=False))
        self.redis.rpush(self.key, datas)

    def get(self, num):
        '''
        从redis获取待用数据
        '''
        key = self.key
        result, ids = [], []
        datas = self.redis.lget(key, num)
        total = self.redis.llen(key)
        for x in datas:
            data = json.loads(x)
            result.append(data)
            if total == 0:
                ids.append(str(data['id']))
        if total == 0:
            self.push(ids)
            more_data = self.redis.lget(key, num - len(result))
            result.extend([json.loads(x)for x in more_data])
        return result