#coding=utf-8
import re
import os
import json
import time
import hashlib
import importlib

def md5(text, coding='utf8'):
    m = hashlib.md5()
    m.update(text.encode(coding))
    return m.hexdigest()

def loadfile(path):
    with open(path) as fp:
        stream = fp.read()
    return stream

def save_file(content, path, filename):
    with open(os.path.join(path, filename), 'wb') as f:
        f.write(content)

def save_json(infodict, path, filename):
    content = json.dumps(infodict, sort_keys=True, ensure_ascii=False, indent=4)
    save_file(content.encode('utf8'), path, filename)

def load_json(path, filename):
    with open(os.path.join(path, filename), 'r') as yf:
        json_data = yf.read()
    json_info = json.loads(json_data)
    return json_info

def get_module(module_name):
    return importlib.import_module(module_name)

def reload_module(module_name):
    module = get_module(module_name)
    return importlib.reload(module)

def get_modules(module_names):
    modules = list()
    for name in module_names:
        modules.append(get_module(name))
    return modules

def get_module_names_from_path(path, exclude=None):
    if exclude is None:
        exclude = []
    exclude.append('__init__.py')
    full_path_names = get_file_names_from_path(path, wanted=['.py'])
    full_names = []
    for name in full_path_names:
        _name = os.path.split(name)[-1]
        if _name in exclude:
            continue
        name = name[:name.rindex('.')]
        full_name = name.replace(os.path.sep, '.')
        full_names.append(full_name)
    return full_names

def get_file_names_from_path(path, wanted=None):
    if path.endswith(os.path.sep):
        path = path[:path.rindex(os.path.sep)]
    gen = os.walk(path)
    full_names = []
    for dirpath, dirnames, filenames in gen:
        if len(filenames) > 0:
            for _name in filenames:
                if wanted is not None and len(wanted) > 0:
                    ext = os.path.splitext(_name)[1]
                    if ext not in wanted:
                        continue
                full_name = os.path.sep.join((dirpath, _name))
                full_names.append(full_name)
    return full_names

try:
    from subprocess import check_output
except ImportError:
    # Python < 2.7 fallback, stolen from the 2.7 stdlib
    def check_output(*popenargs, **kwargs):
        from subprocess import Popen, PIPE, CalledProcessError
        if 'stdout' in kwargs:
            raise ValueError('stdout argument not allowed, it will be overridden.')
        process = Popen(stdout=PIPE, *popenargs, **kwargs)
        output, _ = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = popenargs[0]
            raise CalledProcessError(retcode, cmd, output=output)
        return output

def strftime(t, format_str='%Y-%m-%d %H:%M:%S'):
    return time.strftime(format_str, time.localtime(t))

def strptime(str_day, keywords=None, format_str='%Y-%m-%d %H:%M', mode='datetime', unit=1):
    import datetime
    if len(str_day) == 8:
        str_day = '%s-%s-%s'%(str_day[:4], str_day[4:6], str_day[6:8])
    else:
        keys = [u'年', u'月', u'日', '/', '.']
        if keywords is not None:
            keys.extend(keywords)
        for k in keys:
            str_day = str_day.replace(k, '-')
    if ':' not in str_day:
        str_day = ' '.join((str_day, '00:00'))
    t = time.strptime(str_day, format_str)
    if mode.upper() == 'TIME':
        return t
    elif mode.upper() == 'TIMESTAMP':
        return int(time.mktime(t))*unit
    elif mode.upper() == 'DATETIME':
        return datetime.datetime.fromtimestamp(time.mktime(t))

def delta_day(fir_str_day, sec_str_day):
    d1 = strptime(fir_str_day)
    d2 = strptime(sec_str_day)
    return (d1 - d2).days

def is_day_between(str_day, start_day, end_day):
    if delta_day(str_day, start_day) < 0:
        return False
    if delta_day(str_day, end_day) > 0:
        return False
    return True

def str_quote(s):
    if s is None or isinstance(s, int) or isinstance(s, float):
        return s
    s = s.replace("\\'", "'")
    s = s.replace("'", "''")
    s = s.replace("\\", "\\\\")
    return s

def rm_inject(s):
    '''
    清除请求中带的恶意参数，防止查询数据库时注入攻击
    '''
    for x in ("'", '"', '#'):
        s = s.replace(x, '')
    return s

zh_pattern = re.compile('[\\u4e00-\\u9fa5]+')
def contain_zh(word):
    '''
    判断传入字符串是否包含中文
    param word: 待判断字符串
    return: True:包含中文  False:不包含中文
    '''
    match = zh_pattern.search(word)
    return match

def join_url(domain, url):
    '''
    URL拼接，如果URL不以http开头，则把域名拼接上去
    params  domain:域名
            url:url
    return  拼接后的完整URL
    '''
    if not url.startswith('http'):
        domain = domain[:-1] if domain.endswith('/') else domain
        url = url[1:] if url.startswith('/') else url
        return '/'.join((domain, url))
    return url

emojis = re.compile(
    "(\\ud83d[\\ude00-\\ude4f])|"  # emoticons
    "(\\ud83c[\\udf00-\\uffff])|"  # symbols & pictographs (1 of 2)
    "(\\ud83d[\\u0000-\\uddff])|"  # symbols & pictographs (2 of 2)
    "(\\ud83d[\\ude80-\\udeff])|"  # transport & map symbols
    "(\\ud83c[\\udde0-\\uddff])"  # flags (iOS)
    "+", flags=re.UNICODE)

def clean_emoji(txt):
    return emojis.sub('', txt)

def unescape(word):
    word = json.dumps([word])
    word = word.replace('["','')
    word = word.replace('"]','')
    word = word.replace('\\','%')
    return word.upper()

def timestamp(unit=1000):
    return int(time.time()*unit)

def to_json(data):
    return json.dumps(data, ensure_ascii=False) if data else ''
