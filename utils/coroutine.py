from gevent import monkey
monkey.patch_all()
from gevent.pool import Pool

class Coroutine(object):
    def __init__(self, size=5):
        super(Coroutine, self).__init__()
        self.pool = Pool(size)

    def run(self, method, datas, merge=False):
        itera = self.pool.imap_unordered(method, datas)
        result = []
        while True:
            try:
                item = next(itera)
                if item is not None:
                    is_list = isinstance(item, list) or isinstance(item, tuple)
                    if is_list and merge:
                        result.extend(item)
                    else:
                        result.append(item)
            except StopIteration:
                break
            except Exception as e:
                pass
        return result
        