#coding=utf8



import json
import os.path

from utils.builtin import *

from pymysql.err import *


PATH = 'output'

previous_file = ''
retrials = 0
MAX_RETRIALS = 3

def get_local_jsons(json_type):
    jsons = []
    file_names = get_file_names_from_path(PATH, wanted=['.json'])
    for _name in file_names:
        if json_type not in _name or not os.path.exists(_name):
            continue
        jsons.append(_name)
    return jsons

def write_titles_by_path(db, filename, replace=False):
    data_dict = load_json('', filename)
    if 'datas' in data_dict:
        datas = data_dict['datas']
    else:
        datas = data_dict
    print('adding data from %s...'%filename, end='')
    write_title(db, datas, 'yx_datas', replace)
    print('done!')

def write_title(db, datas, table_name, replace=False):
    global retrials
    conn = db.connect()
    if isinstance(datas, dict):
        datas = list(datas.values())
    try:
        for _v in datas:
            _labels = json.dumps(_v['labels'], ensure_ascii=False)
            _labels = str_quote(_labels)
            _ct_str = strftime(_v['collect_time'], "%Y-%m-%d")
            operator = 'replace' if replace else 'ignore'
            parameters = {
                'operator': operator,
                'table': table_name,
                'values':{
                    'collect_time': str(_v['collect_time']),
                    'ct_str': _ct_str,
                    'data_time': _v['data_time'],
                    'labels': _labels,
                    'md5': _v['md5'],
                    'source': str_quote(_v['source']),
                    'title': str_quote(_v['title']),
                    'url': str_quote(_v['url']),
                }
            }
            sql = db.get_insert_sql(parameters)
            if execute_sql(db, conn, sql, _v['md5']):
                retrials = 0
            else:
                retrials += 1
            if retrials > MAX_RETRIALS:
                break
    finally:
        conn.close()

def write_contents_by_path(db, filenames, replace=False):
    global previous_file
    global retrials
    conn = db.connect()
    try:
        for filename in filenames:
            data_dict = load_json('', filename)
            previous_path = os.path.dirname(previous_file)
            current_path = os.path.dirname(filename)
            if previous_path != current_path:
                if previous_path != '':
                    print('done!')
                print('adding datas from %s...'%current_path, end='')
            if write_content(db, conn, data_dict, 'yx_contents'):
                retrials = 0
            else:
                retrials += 1
            if retrials > MAX_RETRIALS:
                break
            previous_file = filename
    finally:
        conn.close()

def write_content(db, conn, data, table_name, replace=False):
    operator = 'replace' if replace else 'ignore'
    parameters = {
        'operator': operator,
        'table': table_name,
        'values':{
            'collect_time': str(data['collect_time']),
            'data_time': data['data_time'],
            'md5': data['md5'],
            'title': str_quote(data['title']),
            'url': str_quote(data['url']),
            'content':str_quote(data['content']),
            'source': str_quote(data['source']) if 'source' in data else '',
        }
    }
    sql = db.get_insert_sql(parameters)
    res = execute_sql(db, conn, sql, data['md5'])
    return res

def write_except(db, data, table_name, replace=False):
    global retrials
    conn = db.connect()
    operator = 'replace' if replace else 'ignore'
    try:
        for v in data:
            parameters = {
                'operator': operator,
                'table': table_name,
                'values':{
                    'last_time': str(v['last_time']),
                    'url': str_quote(v['url']),
                    'md5': v['md5'],
                    'source': str_quote(v['source']) if 'source' in v else '',
                }
            }
            sql = db.get_insert_sql(parameters)
            if execute_sql(db, conn, sql, md5):
                retrials = 0
            else:
                retrials += 1
            if retrials > MAX_RETRIALS:
                break
    finally:
        conn.close()

def execute_sql(db, conn, sql, id=None):
    try:
        db.excute_sql_once(conn, sql)
    except (UnicodeEncodeError, Error) as e:
        print(e, id)
        return False
    except Warning as e:
        pass
    return True
