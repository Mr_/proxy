class FinishedStop(Exception):
    message = 'Finished Stop Exception'

class NotImplementedInterface(Exception):
    pass
