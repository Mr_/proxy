import logging
import logging.handlers
import os.path

log_path = 'logs'
log_file = 'logs.log'
if not os.path.exists(log_path):
    os.makedirs(log_path)

def log(*msg):
    txts = []
    for x in msg:
        if x is None:
            txts.append('None')
        else:
            txts.append(str(x))
    txt = u' '.join(txts)
    logger = get_log('pro_log')
    logger.info(txt)

def get_log(log_name=None):
    logger = logging.getLogger(log_name)
    logger.setLevel(logging.INFO)
    return logger

def get_file_handler(file, path):
    handler = logging.handlers.RotatingFileHandler(
        os.path.join(path, file),
        'a',
        70*1024*1024,
        2,
    )
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(levelname)s %(asctime)s %(filename)s[line:%(lineno)d] %(message)s'
    )
    handler.setFormatter(formatter)
    return handler

def get_stream_header():
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(levelname)s %(asctime)s %(filename)s[line:%(lineno)d] %(message)s'
    )
    handler.setFormatter(formatter)
    return handler

def init(file=log_file, path=log_path):
    logger = get_log()
    logger.addHandler(get_file_handler(file, path))
    # logger.addHandler(get_stream_header())


