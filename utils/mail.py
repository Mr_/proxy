#coding=utf8
import smtplib

from email.header import Header
from email.utils import formataddr
from email.mime.text import MIMEText

from settings import MAIL_INFO

def send(content, usr=MAIL_INFO['MAIL_USR'], pwd=MAIL_INFO['MAIL_PWD'], host='smtp.qq.com',
        port=465, title='', sender='Lemon', receiver=MAIL_INFO['MAIL_RECEIVER'], sub_type='plain'):
    msg = MIMEText(content, sub_type, 'utf-8')
    msg['From'] = formataddr([sender, usr])
    msg['To'] = formataddr(['', receiver])
    msg['Subject'] = title
    try:
        server = smtplib.SMTP_SSL(host, port) 
        server.ehlo()
        # server.set_debuglevel(1)
        server.login(usr, pwd)
        server.sendmail(usr, [receiver], msg.as_string())
        server.quit()
    except :
        raise Exception('Send Email fialed')

if __name__ == '__main__':
    send(u'''通过简单的四个步骤开始今天的学习
您好，
欢迎来到 Coursera！
在 Coursera，您可以学习世界上最好的大学和授课教师的数以百计的课程。课程对所有人开放，任何人都可以随时、随地学习。请按照以下 4 个简单步骤开始实现您的学习目标，并开发您的潜力。''')
