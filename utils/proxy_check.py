# coding=utf-8

import os
import json

import net.request
import utils.builtin as bu

r = net.request.Request()

LOCAL_IP_PATH = 'logs/local_ip.txt'

def init_local_ip():
    html = r.req('http://httpbin.org/ip')
    data = json.loads(html)
    ori = data.get('origin', '')
    ip = ori.split(',')[0]
    with open(LOCAL_IP_PATH, 'w') as f:
        f.write(ip.strip())
        
if not os.path.exists(LOCAL_IP_PATH):
    init_local_ip()

def check(proxy, https=False, base_ip=None, retry_time=2):
    """
    检验代理是否可用
    Parameters
    ----------
        proxy: str
        代理ip
        https: bool
        是否https代理
        base_ip: str
        本地ip
        retry_time: int
        重试次数
    Returns
    -------
        result: dict
        检查结果，key为hide，有三个结果,-1:不可用,0:非匿名代理,1:高匿代理
    """
    result = {'hide': -1}
    protocol = 'https' if https else 'http'
    proxies = {protocol: f'{protocol}://{proxy}'}
    baidu = f'{protocol}://www.baidu.com'
    params = dict(
        url=baidu,
        proxies=proxies,
        timeout=8,
        retry_time=retry_time,
        retry_interval=0
    )
    page = r.req(**params)
    if page is None:
        return result
    url = f'{protocol}://httpbin.org/ip'
    params['url'] = url
    page = r.req(**params)
    try:
        json.loads(page)['origin']
        result['hide'] = 1 if base_ip not in page else 0
    except Exception as e:
        pass
    return result

def checks(pool, datas, log_str=''):
    '''
    检查代理可用性及匿名度，只保留http/https=1的高匿数据
    '''
    my_ip = bu.loadfile(LOCAL_IP_PATH)

    def single_check(data):
        log_msg = [log_str]
        active = {}
        active.update(data)
        http = check(data['proxy'], base_ip=my_ip)['hide']
        https = check(data['proxy'], https=True, base_ip=my_ip)['hide']
        active['http'], active['https'] = http, https
        if http == 1 or https == 1:
            log_msg.append('Pass')
        else:
            active = None
            log_msg.append('Err')
        log_msg.extend([
            data['proxy'],
            'http:%d' %http if http > -1 else '',
            'https:%d' %https if https > -1 else '',
        ])
        print(' '.join(log_msg))
        return active

    result = pool.run(single_check, datas)
    return result

def check_pos(pool, datas):
    def single_check(data):
        country = data.get('country', None)
        if country is not None and country != '':
            return
        data['country'] = ''
        data['region'] = ''
        data['city'] = ''
        data['isp'] = ''
        data['inside'] = 1
        # for x in range(3):
        #     html = r.req(
        #         'http://ip.taobao.com/service/getIpInfo.php?',
        #         timeout=10,
        #         params=dict(ip=data['proxy'].split(':')[0]),
        #         retry_time=3,
        #         retry_interval=1,
        #     )
        #     if html is None:
        #         continue
        #     res = json.loads(html)
        #     res = res['data']
        #     try:
        #         res.get('country', '')
        #     except Exception as e:
        #         continue
        #     data['country'] = res.get('country', '')
        #     data['region'] = res.get('region', '')
        #     data['city'] = res.get('city', '')
        #     data['isp'] = res.get('isp', '')[:10]
        #     if data['country'] == '中国':
        #         data['inside'] = 1
        #     else:
        #         data['inside'] = 0
        #     break
        # else:
        #     data['country'] = ''
        #     data['region'] = ''
        #     data['city'] = ''
        #     data['isp'] = ''
    pool.run(single_check, datas)
    # for br in datas:
    #     single_check(br)
    
if __name__ == '__main__':
    print(check('52.35.63.184:8080', True, '113.108.138.153'))