import re

import openpyxl

ILLEGAL_CHARACTERS = re.compile(r'[\000-\010]|[\013-\014]|[\016-\037]')

def save(name, datas):
    wb = openpyxl.Workbook()
    wb.remove(wb.active)
    for item in datas:
        sheet_name, data = item['name'], item['data']
        table = wb.create_sheet(sheet_name)
        for i, row in enumerate(data):
            for j, col in enumerate(row):
                if col != '':
                    if col is not None:
                        content = ILLEGAL_CHARACTERS.sub(r'', col)
                    table.cell(i+1, j+1).value = content
    wb.save(name)
    print('Save %s success ...' %name)
