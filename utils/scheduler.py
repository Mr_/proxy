#coding=utf8

import json
import os.path
import datetime
import importlib

import apscheduler.events

from collections import OrderedDict

from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import IntervalTrigger
# from apscheduler.schedulers.blocking import BlockingScheduler 
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor

import settings
import utils.builtin
import storage.dbinterface

SUCCESSFUL = 'successful'
FAILED = 'failed'
MISSED = 'missed'
SUBMITTED = 'summited'

JOB_TABLE = 'sch_jobs'
LOG_TABLE = 'sch_logs'

INIT_JOB_SQL = '''
CREATE TABLE IF NOT EXISTS %s (
id INTEGER PRIMARY KEY AUTOINCREMENT,
name TEXT NOT NULL,
runs INT NOT NULL,
fails INT NOT NULL,
misseds INT NOT NULL,
paused INT NOT NULL,
isrunning INT NOT NULL,
internal_id TEXT NOT NULL,
last_duration TEXT,
last_start_time TEXT,
last_status TEXT,
schedule TEXT NOT NULL)
''' %JOB_TABLE

INIT_LOG_SQL = '''
CREATE TABLE IF NOT EXISTS %s (
id INTEGER PRIMARY KEY AUTOINCREMENT,
jid INT NOT NULL,
build_num INT NOT NULL,
duration TEXT NOT NULL,
message TEXT NOT NULL,
start_time TEXT NOT NULL,
status TEXT NOT NULL);
''' %LOG_TABLE

TIME_FORMATER = '%Y-%m-%d %H:%M:%S'

# def encode_job_name(name):
#     return name.replace('.', '_')

# def decode_job_name(name):
#     return name.replace('_', '.')

def trigger_to_dict(trigger):
    data = OrderedDict()
    if isinstance(trigger, DateTrigger):
        data['trigger'] = 'date'
        data['run_date'] = trigger.run_date.strftime(TIME_FORMATER)
    elif isinstance(trigger, IntervalTrigger):
        data['trigger'] = 'interval'
        data['start_date'] = trigger.start_date.strftime(TIME_FORMATER)
        if trigger.end_date:
            data['end_date'] = trigger.end_date.strftime(TIME_FORMATER)
        w, d, hh, mm, ss = extract_timedelta(trigger.interval)
        if w > 0:
            data['weeks'] = w
        if d > 0:
            data['days'] = d
        if hh > 0:
            data['hours'] = hh
        if mm > 0:
            data['minutes'] = mm
        if ss > 0:
            data['seconds'] = ss
    elif isinstance(trigger, CronTrigger):
        data['trigger'] = 'cron'
        if trigger.start_date:
            data['start_date'] = trigger.start_date.strftime(TIME_FORMATER)
        if trigger.end_date:
            data['end_date'] = trigger.end_date.strftime(TIME_FORMATER)
        for field in trigger.fields:
            if not field.is_default:
                data[field.name] = str(field)
    else:
        data['trigger'] = str(trigger)
    return data

def extract_timedelta(delta):
    w, d = divmod(delta.days, 7)
    mm, ss = divmod(delta.seconds, 60)
    hh, mm = divmod(mm, 60)
    return w, d, hh, mm, ss


class Scheduler(BackgroundScheduler):
# class Scheduler(BlockingScheduler):
    def __init__(self, executors, log_path, log_file):
        super(Scheduler, self).__init__(executors=executors)
        self.add_listener(self.job_listener,
                           apscheduler.events.EVENT_JOB_ERROR |
                           apscheduler.events.EVENT_JOB_MISSED |
                           apscheduler.events.EVENT_JOB_EXECUTED |
                           apscheduler.events.EVENT_JOB_SUBMITTED)
        # self.max_log = 5
        self.init_db(os.path.join(log_path, log_file))

    def init_db(self, path):
        db_cfg = {'sqllite_path': path}
        self.db = storage.dbinterface.DBInterface(db_cfg)
        self.db.excute_sql(INIT_JOB_SQL)
        self.db.excute_sql(INIT_LOG_SQL)

    def mark(self, event, status):
        # print(event, status)
        job_info = self.get_job_by_iid(event.job_id)
        if job_info is None:
            return
        if status == SUBMITTED:
            start_time = event.scheduled_run_times[0].replace(microsecond=0)
            start_time = start_time.strftime(TIME_FORMATER)
            if start_time == job_info.get('last_start_time', '@'):
                return
            self.up_job_info(job_info, {'isrunning': 1})
            return
        up_info = {'runs': job_info['runs'] + 1}
        start_time = event.scheduled_run_time.replace(microsecond=0)
        now = datetime.datetime.now().replace(microsecond=0)
        duration = str(now - start_time.replace(tzinfo=None))
        if status == SUCCESSFUL:
            message = 'Job run successfully'
        elif status == FAILED:
            up_info['fails'] = job_info['fails'] + 1
            message = '\n'.join(('Job raised an exception',
                                  event.traceback,
                                  event.exception.__repr__(),))
        elif status == MISSED:
            up_info['misseds'] = job_info['misseds'] + 1
            message = 'Run time of job was missed'
        else:
            message = 'Unknown Job status'
        start_time = start_time.strftime(TIME_FORMATER)
        log = {
            'jid': job_info['id'],
            'build_num': up_info['runs'],
            'start_time': start_time,
            'duration': duration,
            'status': status,
            'message': message,
        }
        self.db.push_data(LOG_TABLE, [log])
        up_info['last_start_time'] = start_time
        up_info['last_duration'] = duration
        up_info['last_status'] = status
        up_info['isrunning'] = 0
        self.up_job_info(job_info, up_info)

    # def clean_logs(self, logs):
    #     if len(logs) > self.max_log:
    #         keys = list(logs.keys()).sort(key=lambda x:int(x))
    #         pop_keys = keys[:len(keys) - self.max_log]
    #         for pop_key in pop_keys:
    #             logs.pop(pop_key)

    def job_listener(self, event):
        if event.code == apscheduler.events.EVENT_JOB_EXECUTED:
            self.mark(event, SUCCESSFUL)
        elif event.code == apscheduler.events.EVENT_JOB_ERROR:
            self.mark(event, FAILED)
        elif event.code == apscheduler.events.EVENT_JOB_MISSED:
            self.mark(event, MISSED)
        elif event.code == apscheduler.events.EVENT_JOB_SUBMITTED:
            self.mark(event, SUBMITTED)

    def add_job_by(self, module_name, reload_module=False):
        module = utils.builtin.get_module(module_name)
        if reload_module:
            importlib.reload(module)
        if hasattr(module, 'schedule_plan'):
            plan = module.schedule_plan
        else:
            plan = settings.SCH_DEF_PLAN
        self.add_job(module.schedule_job, plan, module_name)

    def add_job(self, func, plan, name, arguments=None, kwarguments=None):
        if arguments is None:
            arguments = []
        if kwarguments is None:
            kwarguments = {}
        if not isinstance(plan, list):
            plan = [plan]
        for each in plan:
            settings = dict(each)
            settings['name'] = name
            if 'trigger' not in settings:
                settings['trigger'] = 'cron'
            if 'misfire_grace_time' not in settings:
                settings['misfire_grace_time'] = 1800
            job = super(Scheduler, self).add_job(
                func,
                args=arguments,
                kwargs=kwarguments,
                **settings)
            db_job = self.get_job_from_db(name)
            if db_job is None:
                job_info = {
                    'name': job.name,
                    'runs': 0,
                    'fails': 0,
                    'misseds': 0,
                    'isrunning': 0,
                    'paused': 0,
                    'internal_id': job.id,
                    'schedule': json.dumps(trigger_to_dict(job.trigger)),
                    'last_duration': '',
                    'last_start_time': '',
                    'last_status': '',
                }
                self.db.push_data(JOB_TABLE, [job_info])
            else:
                job_info = {
                    'id': db_job['id'],
                    'internal_id': job.id,
                    'isrunning': 0,
                    'schedule': json.dumps(trigger_to_dict(job.trigger)),
                }
                if db_job['paused'] == 1:
                    super(Scheduler, self).pause_job(job.id)
                self.db.update_data(JOB_TABLE, [job_info])

    def get_job_from_db(self, name):
        '''
        根据name从数据库获取job信息
        '''
        sql = 'select * from %s where name="%s"' %(JOB_TABLE, name)
        res = self.db.excute_sql(sql)
        return res[0] if len(res) > 0 else None

    def get_job_by_id(self, id):
        '''
        根据id从数据库获取job信息
        '''
        sql = 'select * from %s where id="%s"' %(JOB_TABLE, id)
        res = self.db.excute_sql(sql)
        if len(res) > 0:
            return self.format_db_job(res[0])
        return None

    def get_job_by_iid(self, iid):
        '''
        根据internal_id从数据库获取job信息
        '''
        sql = 'select * from %s where internal_id="%s"' %(JOB_TABLE, iid)
        res = self.db.excute_sql(sql)
        if len(res) > 0:
            return self.format_db_job(res[0])
        return None

    def format_db_job(self, job):
        '''
        将job从数据库数据结构转化成schedule可用结构
        '''
        job = {k: v for k,v in job.items()}
        if 'isrunning' in job:
            job['isrunning'] = True if job['isrunning'] == 1 else False
        if 'schedule' in job:
            job['schedule'] = json.loads(job['schedule'])
        if 'paused' in job:
            job['paused'] = True if job['paused'] == 1 else False
        return job

    def build_instant_job(self, id, args=None, kwargs=None):
        '''
        立刻启动一次job
        '''
        self.reload_job(id)
        db_info = self.get_job_by_id(id)
        schd_iid = db_info['internal_id']
        job = self.get_job(schd_iid)
        job._modify(next_run_time=datetime.datetime.now())
        for jobstore in list(self._jobstores.values()):
            if jobstore.lookup_job(schd_iid):
                jobstore.update_job(job)
        self.wakeup()
        return db_info

    def reload_job(self, id):
        '''
        重新加载job
        '''
        db_info = self.get_job_by_id(id)
        schd_iid = db_info['internal_id']
        if self.get_job(schd_iid) is not None:
            self.remove_job(schd_iid)
        job_name = db_info['name']
        self.add_job_by(db_info['name'], reload_module=True)
        return db_info

    def up_job_info(self, job, info):
        '''
        更新数据库内job信息
        '''
        info['id'] = job['id']
        self.db.update_data(JOB_TABLE, [info])

    def update_job(self, id, trigger=None, trigger_args=None):
        '''
        更新job的时间配置
        '''
        db_info = self.get_job_by_id(id)
        schd_iid = db_info['internal_id']
        job = self.reschedule_job(schd_iid, trigger=trigger,
                                             **trigger_args)
        db_info['schedule'] = trigger_to_dict(job.trigger)
        up_info = {'schedule': json.dumps(db_info['schedule'])}
        self.up_job_info(db_info, up_info)
        return db_info

    def pause_job(self, id):
        '''
        暂停
        '''
        db_info = self.get_job_by_id(id)
        schd_iid = db_info['internal_id']
        super(Scheduler, self).pause_job(schd_iid)
        db_info['paused'] = True
        self.up_job_info(db_info, {'paused': 1})
        return db_info

    def resume_job(self, id):
        '''
        恢复job
        '''
        db_info = self.get_job_by_id(id)
        schd_iid = db_info['internal_id']
        super(Scheduler, self).resume_job(schd_iid)
        db_info['paused'] = False
        self.up_job_info(db_info, {'paused': 0})
        return db_info

    def job_list(self):
        sql = 'select * from %s' %JOB_TABLE
        jobs = self.db.excute_sql(sql)
        jobs = [self.format_db_job(x)for x in jobs]
        return jobs

    def format_db_log(self, log):
        '''
        将log从数据库数据结构转化成schedule可用结构
        '''
        return {k: v for k,v in log.items()}

    def job_log(self, id, build_num):
        sql = 'select * from %s where jid=%s and build_num=%s' %(
            LOG_TABLE,
            id,
            build_num,
        )
        log = self.db.excute_sql(sql)
        if len(log) > 0:
            return self.format_db_log(log[0])
        return {}

    def job_log_list(self, id):
        sql = 'select * from %s where jid=%s order by id desc limit 100' %(LOG_TABLE, id)
        logs = self.db.excute_sql(sql)
        if len(logs) > 0:
            return tuple(self.format_db_log(x)for x in logs)
        return {}
