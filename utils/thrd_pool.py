from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

from utils.coroutine import Coroutine

class Thrd_pool(object):
    '''
    线程池封装，每个线程里包了一个协程池
    params:
        thrd_size(int) 线程池并发上限
        coro_size(int) 单个协程池并发上限
    '''
    def __init__(self, thrd_size=1, coro_size=50):
        super(Thrd_pool, self).__init__()
        self.pool = ThreadPoolExecutor(max_workers=thrd_size)
        self.coros = tuple(Coroutine(coro_size)for x in range(thrd_size))
        self.thrd_size = thrd_size
        # self.coro_size = coro_size

    def run_func(self, args):
        return args['coro'].run(**args['args'])

    def run(self, method, datas, merge=False):
        result = []
        num = len(datas)
        if num == 0:
            return result
        size = num // self.thrd_size
        range_num = 1 if size < 1 else self.thrd_size
        args = []
        for x in range(range_num):
            start = x * size
            end = (x + 1) * size if x + 1 < range_num else num
            arg = {
                'merge': merge,
                'method': method,
                'datas': datas[start: end],
                }
            args.append({'coro': self.coros[x], 'args': arg})
        for data in self.pool.map(self.run_func, args):
            result.extend(data)
        return result

def run_func(args):
    return args['coro'].run(**args['args'])
        
class Proc_pool(object):
    '''
    进程池封装，每个线程里包了一个协程池
    params:
        thrd_size(int) 进程池并发上限
        coro_size(int) 单个协程池并发上限
    '''
    def __init__(self, thrd_size=1, coro_size=50):
        super(Proc_pool, self).__init__()
        self.pool = ProcessPoolExecutor(max_workers=thrd_size)
        self.coros = tuple(Coroutine(coro_size)for x in range(thrd_size))
        self.thrd_size = thrd_size

    def run(self, method, datas, merge=False):
        result = []
        num = len(datas)
        if num == 0:
            return result
        size = num // self.thrd_size
        range_num = 1 if size < 1 else self.thrd_size
        args = []
        for x in range(range_num):
            start = x * size
            end = (x + 1) * size if x + 1 < range_num else num
            arg = {
                'merge': merge,
                'method': method,
                'datas': datas[start: end],
                }
            args.append({'coro': self.coros[x], 'args': arg})
        print('aaa')
        for data in self.pool.map(run_func, args):
            result.extend(data)
        print('bbb')
        return result

class Pool(object):
    def __init__(self, size):
        super(Pool, self).__init__()
        self.pool = ThreadPoolExecutor(max_workers=size)

    def run(self, method, datas, merge=False):
        result = []
        for data in self.pool.map(method, datas):
            if merge:
                result.extend(data)
            else:
                result.append(data)
        return result

import time

def aa(data):
    time.sleep(data['sleep'])
    print('idx:{idx}, sleep:{sleep}'.format(**data))
    return data

def test():
    import random

    datas = []
    for x in range(random.randint(1, 3)):
        datas.append({
            'idx': x,
            'sleep': random.randint(1,5)    
        })
    pool = Proc_pool(5)
    xx = pool.run(aa, datas)
    # pool = ProcessPoolExecutor(max_workers=5)
    # xx = []
    # for data in pool.map(aa, datas):
    #     xx.append(data)
    print(len(xx))
    import ipdb;ipdb.set_trace()
    pass

if __name__ == '__main__':
    test()
