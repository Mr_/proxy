import os
from flask import Flask, current_app

from gevent import pywsgi
from gevent import monkey
monkey.patch_all()

import utils.log

from storage.dbinterface import DBInterface

def start_spider(host='', port=5001, args=None):
    '''
    启动爬虫
    '''
    from webapp.bp_manage import BlueprintManager
    utils.log.init('spider.log')
    app = Flask(__name__)
    app.config.from_object('settings')
    app.config['SVC_DB'] = DBInterface()
    bpm = BlueprintManager('manage', __name__, static_url_path='',
            static_folder='bp_manage/static')
    app.register_blueprint(bpm, url_prefix='/manage')
    bpm.start(args)
    http_server = pywsgi.WSGIServer((host, port), app)
    print(('http://127.0.0.1:%d' %port))
    print(('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C')))
    http_server.serve_forever()

def start_api(host='', port=5001, args=None):
    '''
    启动接口
    '''
    from webapp.bp_query import BlueprintQuery
    utils.log.init('api.log')
    app = Flask(__name__)
    app.config.from_object('settings')
    app.config['SVC_DB'] = DBInterface()
    bpq = BlueprintQuery('query', __name__, static_url_path='',
            static_folder='bp_query/static')
    app.register_blueprint(bpq)
    bpq.start(args)
    http_server = pywsgi.WSGIServer((host, port), app)
    print(('http://127.0.0.1:%d' %port))
    print(('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C')))
    http_server.serve_forever()
    
def shutdown():
    for bp in list(current_app.blueprints.values()):
        bp.shutdown()

if __name__ == '__main__':
    start_spider()