''' manage UI blueprint initialization '''

import flask

from . import web_scheduler


class BlueprintManager(flask.Blueprint):
    def __init__(self, *args, **kwargs):
        super(BlueprintManager, self).__init__(*args, **kwargs)
        web_scheduler.initialize(self)

    def start(self, args=None):
        web_scheduler.start_scheduler(args)

    def shutdown(self):
        web_scheduler.shutdown_scheduler()
