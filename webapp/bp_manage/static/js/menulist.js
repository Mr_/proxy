var nav=document.getElementById("nav").getElementsByTagName("li");
for(i=0;i<nav.length;i++) {
    nav[i].onmouseover=function() {
        this.style.fontWeight="bold";
        this.style.overflow="visible";
        this.style.background="white";
    }
    nav[i].onmouseout=function() {
        this.style.fontWeight="normal";
        this.style.background="white"
        this.style.overflow="hidden";
    }
}

function menu_change(obj) {
    var val = obj.getAttribute("value");
    if(val == "crawler") {
        location.href = "./home";
    } else {
        location.href = "./maxdate";
    }
}
