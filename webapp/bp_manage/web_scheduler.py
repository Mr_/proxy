# coding=utf8

from flask import current_app
from flask_restful import Resource, Api, request, abort

import utils.builtin as bu
import utils.scheduler as sc

from settings import *

def create_scheduler():
    scdr = sc.Scheduler(
        {
            'default': sc.ThreadPoolExecutor(20),
            'processpool': sc.ProcessPoolExecutor(2),
        },
        SCH_LOG_PATH,
        SCH_LOG_FILE)
    if SCH_JOB_PATH is not None:
        names = bu.get_module_names_from_path(SCH_JOB_PATH, SCH_JOB_EXCLUDES)
        SCH_JOBS.extend(names)
    [scdr.add_job_by(module_name)for module_name in SCH_JOBS]
    return scdr

def start_scheduler(args=None):
    if not scheduler.running:
        scheduler.start()

def shutdown_scheduler():
    schedule.shutdown()

scheduler = create_scheduler()
local_app = None

class Home(Resource):
    """
    Home
    """
    def get(self):
        """
            Html to show crawler list
        """
        cks = request.headers.get('Cookie', '')
        if BOSS_KEY not in cks:
            return ''
        return local_app.send_static_file('crawler.html')

class LogPage(Resource):
    """
    LogPage
    """
    def get(self):
        """
            Html to show log list
        """
        return local_app.send_static_file('log.html')

class RestartScheduler(Resource):
    """
    startSchedulerApi
    """
    def post(self):
        """
            Restart the scheduler, all running job will be stopped and
            loaded from module.
            Return 500 code with massage if exception happens.
        """
        global scheduler
        if scheduler.running:
            scheduler.shutdown(wait=False)
        try:
            scheduler_executors = get_executors()
            scheduler = create_scheduler(scheduler_executors, SCH_LOG_PATH,
                                            SCH_LOG_FILE)
            add_jobs()
            scheduler.start()
            return {'status': '200',
                    'message': 'restart the scheduler successfully'}
        except Exception as e:
            abort(500, message=str(e))


class Joblist(Resource):
    """
    JoblistApi
    """
    def get(self):
        """
            Return job list added to scheduler
        """
        data = scheduler.job_list()
        return {'status': '200', 'data':data}

class Joblog(Resource):
    """
    JoblogApi
    """
    def get(self, job_id, build_num):
        """
            Return job log of the given job id and build number.
            Return 400 code with massage if wrong job_id or build_num
            are given.
            :param str|int job_id: job id(started from 1, not internal id)
            :param str|int build_num: build number of the job
        """
        data = scheduler.job_log(str(job_id), str(build_num))
        if len(data) > 0:
            return {'status': 200, 'data': data}
        else:
            abort(400, message='Invalid job/build number')

class Jobloglist(Resource):
    """
    JobloglistApi
    """
    def get(self, job_id):
        """
            Return job log list of the given job id.
            Return 400 code with massage if wrong job_id is given.
            :param str|int job_id: job id(started from 1, not internal id)
        """
        data = scheduler.job_log_list(str(job_id))
        if len(data) > 0:
            return {'status': 200, 'data': data}
        else:
            abort(400, message='Invalid job number')

class PauseJob(Resource):
    """
    PauseJobApi
    """
    def post(self, job_id):
        """
            Pause a job and return latest info of this job
            after pause action completed.
            Return 500 code with massage if wrong job_id is given.
            :param str|int job_id: job id(started from 1, not internal id)
        """
        try:
            result = scheduler.pause_job(str(job_id))
            return {'status': '200', 'data': result}
        except Exception as e:
            abort(500, message=str(e))

class ResumeJob(Resource):
    """
    ResumeJobApi
    """
    def post(self, job_id):
        """
            Resume a job from paused and return latest info of this job
            after resume action completed.
            Return 500 code with massage if wrong job_id is given.
            :param str|int job_id: job id(started from 1, not internal id)
        """
        try:
            result = scheduler.resume_job(str(job_id))
            return {'status': '200', 'data': result}
        except Exception as e:
            abort(500, message=str(e))

class BuildJob(Resource):
    """
    BuildJobApi
    """
    def post(self, job_id):
        """
            Schedule an instant build to a job and return latest info
            of this job after build action scheduled.
            Note: the 'internal_ids' list will be increased by the latest
            build job internal id.
            Return 500 code with massage if wrong job_id is given.
            :param str|int job_id: job id(started from 1, not internal id)
        """
        try:
            result = scheduler.build_instant_job(str(job_id))
            return {'status': '200', 'data': result}
        except Exception as e:
            abort(500, message=str(e))

class UpdateJob(Resource):
    """
    UpdateJobApi
    """
    def post(self, job_id):
        """
            Reschedule a job and return latest info of this job after
            rescheduled.
            Return 500 code with massage if wrong job_id is given.
            :param str|int job_id: job id(started from 1, not internal
            id)
            A job can be rescheduled by 3 general triggers:
            'date': triggers once on the given datetime
            'internal': triggers on specified intervals
            'cron': triggers when current time matches all specified
            time constraints, similarly to how the UNIX cron scheduler
            works.
            the trigger will be set in the post request dictionary,
            simple format is like:
            'date': {'trigger': 'date', 'run_date': '2017-07-14 08:00:00'}
            will reschedule the job to run once at 8 am of July 14, 2017
            or
            'interval': {'trigger': 'interval', 'hours': 5}
            will reschedule the job to run every 5 hours. notice
            value of 'hours'/'minutes'/'second's need to be set to int
            instead of str
            or
            'cron': {'trigger': 'cron', 'hour': '0,8,13,20',
                    'day_of_week': '0-4'}
            will reschedule the job to run at 00:00, 08:00, 13:00 and
            20:00 from Monday to Friday of every week.
            for more detials of the trigger setting, please refer to
            the triggers part of apschedule document:
            http://apscheduler.readthedocs.io/en/latest/userguide.html#
        """
        data = request.get_json(force=True)
        trigger_name = data.pop('trigger')
        trigger_args = {}

        if trigger_name == 'date':
            trigger_arg_names = ('run_date', 'timezone')
        elif trigger_name == 'interval':
            trigger_arg_names = ('weeks', 'days', 'hours', 'minutes',
                                    'seconds', 'start_date', 'end_date',
                                    'timezone')
        elif trigger_name == 'cron':
            trigger_arg_names = ('year', 'month', 'day', 'week',
                                    'day_of_week', 'hour', 'minute', 'second',
                                    'start_date', 'end_date', 'timezone')
        else:
            abort(500, message='Trigger %s is not supported.' % trigger_name)

        for arg_name in trigger_arg_names:
            if arg_name in data:
                trigger_args[arg_name] = data.pop(arg_name)

        job_id = str(job_id)
        try:
            result = scheduler.update_job(job_id, trigger_name,
                                                    trigger_args)
            return {'status': '200', 'data': result}
        except Exception as e:
            abort(500, message=str(e))

def initialize(app):
    global local_app
    if local_app is None:
        local_app = app
    api = Api(app)
    api.add_resource(Home, '/', '/home')
    api.add_resource(LogPage, '/logpage')
    api.add_resource(Joblist, '/crawler', '/crawler/list')
    api.add_resource(RestartScheduler, '/crawler/restart')
    api.add_resource(Joblog, '/crawler/job/<string:job_id>/log/<string:build_num>')
    api.add_resource(Jobloglist, '/crawler/job/<string:job_id>/log')
    api.add_resource(PauseJob, '/crawler/job/<string:job_id>/pause')
    api.add_resource(ResumeJob, '/crawler/job/<string:job_id>/resume')
    api.add_resource(BuildJob, '/crawler/job/<string:job_id>/build')
    api.add_resource(UpdateJob, '/crawler/job/<string:job_id>/update')
