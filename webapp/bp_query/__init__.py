#coding=utf8
from flask import Blueprint

from webapp.bp_query.restful import initialization


class BlueprintQuery(Blueprint):
    def __init__(self, *args, **kwargs):
        super(BlueprintQuery, self).__init__(*args, **kwargs)
        initialization.initialize(self)

    def start(self, args=None):
        pass

    def shutdown(self):
        pass
