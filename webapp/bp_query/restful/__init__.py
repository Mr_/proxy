import flask

def get_current_bp():
    return flask.current_app.blueprints['query']
