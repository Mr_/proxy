import flask

from flask_restful import reqparse
from flask_restful import Resource

from . import get_current_bp


class HOME(Resource):
    def __init__(self):
        super(HOME, self).__init__()
        
    def get(self):
        return get_current_bp().send_static_file('home.html')

class ALL(Resource):
    def __init__(self):
        super(ALL, self).__init__()
        self.svc_db = flask.current_app.config['SVC_DB']

    def get(self):
        params = {
            'select': ['*'],
            'from': ['proxies'],
            'order_by': (('id', 'DESC'),),
        }
        sql = self.svc_db.get_sql(params)
        res = self.svc_db.excute_sql(sql)
        result = []
        for r in res:
            result.append({k:v for k, v in list(r.items())})
        return {'code': 200, 'datas': result}
        
class GET(Resource):
    def __init__(self):
        super(GET, self).__init__()
        self.svc_db = flask.current_app.config['SVC_DB']
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('http')
        self.reqparser.add_argument('https')
        self.reqparser.add_argument('inside')
        self.reqparser.add_argument('size')

    def get(self):
        args = self.reqparser.parse_args()
        sql = ['SELECT id, proxy, http, https, inside FROM proxies']
        # sql.append('WHERE id >= ((SELECT MAX(id) FROM proxies)-(SELECT MIN(id) FROM proxies)) * RAND() + (SELECT MIN(id) FROM proxies) and del>=0')
        sql.append('where del>=0')
        for arg in ('http', 'https', 'inside'):
            value = args[arg]
            if value is not None and value != '':
                sql.append('and {}={}'.format(arg, value))
        sql.append('order by rand()')
        size = 10 if args['size'] is None else args['size']
        sql.append('LIMIT {};'.format(size))
        res = self.svc_db.excute_sql(' '.join(sql))
        result = [dict(x)for x in res]
        # for r in res:
        #     result.append({k:v for k, v in list(r.items())})
        return {'code': 200, 'datas': result}
