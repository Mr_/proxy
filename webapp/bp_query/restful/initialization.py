from flask_restful import Api, Resource

from webapp.bp_query.restful.home import *
from webapp.bp_query.restful.scan_ip import Scan_ip

def initialize(app):
    api = Api(app)
    api.add_resource(HOME, '/')
    api.add_resource(ALL, '/all')
    api.add_resource(GET, '/get')
    api.add_resource(Scan_ip, '/scan_ip')

