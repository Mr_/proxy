import queue
import datetime

import flask

import utils.builtin

from flask_restful import Resource



class Ip_manager(object):
    '''
    IP分发器
    '''
    CFG_PATH = 'sources'
    CFG_NAME = 'ip.cfg'

    def __init__(self):
        super(Ip_manager).__init__()
        cfg = utils.builtin.load_json(self.CFG_PATH, self.CFG_NAME)
        self.ips = cfg['ips']
        self.queues = []
        for idx,ip in enumerate(self.ips):
            item = queue.Queue()
            [item.put(x)for x in range(ip+1, 256)]
            [item.put(x)for x in range(1 if idx == 0 else 0, ip+1)]
            self.queues.append(item)

    def up_cfg(self):
        info = {
            'ips': self.ips,
            'time': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        }
        utils.builtin.save_json(info, self.CFG_PATH, self.CFG_NAME)

    def new_ip(self, pos):
        ip = self.queues[pos].get()
        self.ips[pos] = ip
        if pos >= 1 and ip == 0:
           self.new_ip(pos - 1) 
        self.queues[pos].put(ip)

    def get(self):
        self.new_ip(2)
        part_if = '{0}.{1}.{2}'.format(*self.ips)
        self.up_cfg()
        return part_if

class Scan_ip(Resource):

    def __init__(self):
        super(Scan_ip, self).__init__()

    def get(self):
        return utils.builtin.load_json(Ip_manager.CFG_PATH, Ip_manager.CFG_NAME)

    def post(self):
        manager = flask.current_app.config.get('ip_manager', None)
        if manager is None:
            manager = Ip_manager()
            flask.current_app.config['ip_manager'] = manager
        return {'code': 200, 'data': manager.get()}

if __name__ == '__main__':
    for x in range(500):
        m = Ip_manager()
        print(m.get())